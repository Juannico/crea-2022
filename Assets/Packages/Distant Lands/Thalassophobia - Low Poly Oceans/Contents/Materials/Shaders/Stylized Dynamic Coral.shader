// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Distant Lands/Stylized Dynamic Coral"
{
	Properties
	{
		[HDR]_TopColor("Top Color", Color) = (0.3160377,1,0.695684,1)
		[HDR]_MainColor("Main Color", Color) = (0.3160377,1,0.695684,1)
		[HDR]_Emmision("Emmision", Color) = (0,0,0,1)
		_MainWaveAmount("Main Wave Amount", Float) = 0.3
		_WaveSpeed("Wave Speed", Float) = 0.5
		_MainWaveScale("Main Wave Scale", Float) = 1
		_GradientSmoothness1("Gradient Smoothness", Float) = 0.5
		_WaveHeightMultiplier("Wave Height Multiplier", Float) = 1
		_FlutterAmount("Flutter Amount", Float) = 0.3
		_GradientOffset1("Gradient Offset", Float) = 0
		_FlutterSpeed("Flutter Speed", Float) = 0.5
		_FlutterScale("Flutter Scale", Float) = 1
		_opacityValue("opacityValue", Range( 0 , 1)) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
		};

		uniform float _FlutterAmount;
		uniform float _FlutterSpeed;
		uniform float _FlutterScale;
		uniform float _MainWaveAmount;
		uniform float _WaveHeightMultiplier;
		uniform float _WaveSpeed;
		uniform float _MainWaveScale;
		uniform float4 _MainColor;
		uniform float4 _TopColor;
		uniform float _GradientOffset1;
		uniform float _GradientSmoothness1;
		uniform float4 _Emmision;
		uniform float _opacityValue;


		float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }

		float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }

		float snoise( float3 v )
		{
			const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
			float3 i = floor( v + dot( v, C.yyy ) );
			float3 x0 = v - i + dot( i, C.xxx );
			float3 g = step( x0.yzx, x0.xyz );
			float3 l = 1.0 - g;
			float3 i1 = min( g.xyz, l.zxy );
			float3 i2 = max( g.xyz, l.zxy );
			float3 x1 = x0 - i1 + C.xxx;
			float3 x2 = x0 - i2 + C.yyy;
			float3 x3 = x0 - 0.5;
			i = mod3D289( i);
			float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
			float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
			float4 x_ = floor( j / 7.0 );
			float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
			float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 h = 1.0 - abs( x ) - abs( y );
			float4 b0 = float4( x.xy, y.xy );
			float4 b1 = float4( x.zw, y.zw );
			float4 s0 = floor( b0 ) * 2.0 + 1.0;
			float4 s1 = floor( b1 ) * 2.0 + 1.0;
			float4 sh = -step( h, 0.0 );
			float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
			float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
			float3 g0 = float3( a0.xy, h.x );
			float3 g1 = float3( a0.zw, h.y );
			float3 g2 = float3( a1.xy, h.z );
			float3 g3 = float3( a1.zw, h.w );
			float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
			g0 *= norm.x;
			g1 *= norm.y;
			g2 *= norm.z;
			g3 *= norm.w;
			float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
			m = m* m;
			m = m* m;
			float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
			return 42.0 * dot( m, px);
		}


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float temp_output_59_0 = ( _FlutterSpeed * _Time.y );
			float3 appendResult63 = (float3(temp_output_59_0 , temp_output_59_0 , temp_output_59_0));
			float3 temp_output_64_0 = ( ase_worldPos + appendResult63 );
			float temp_output_62_0 = ( 1.0 / _FlutterScale );
			float simplePerlin3D70 = snoise( temp_output_64_0*temp_output_62_0 );
			float simplePerlin3D69 = snoise( temp_output_64_0*( temp_output_62_0 * 0.5 ) );
			float3 appendResult67 = (float3(simplePerlin3D70 , 0.0 , simplePerlin3D69));
			float4 transform52 = mul(unity_ObjectToWorld,float4( 0,0,0,1 ));
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 appendResult51 = (float3(transform52.x , ( ase_vertex3Pos.y * _WaveHeightMultiplier ) , transform52.z));
			float temp_output_36_0 = ( _WaveSpeed * _Time.y );
			float3 appendResult38 = (float3(temp_output_36_0 , temp_output_36_0 , temp_output_36_0));
			float3 temp_output_39_0 = ( appendResult51 + appendResult38 );
			float temp_output_16_0 = ( 1.0 / _MainWaveScale );
			float simplePerlin2D7 = snoise( temp_output_39_0.xy*temp_output_16_0 );
			float simplePerlin3D8 = snoise( temp_output_39_0*( temp_output_16_0 * 0.8 ) );
			float3 appendResult17 = (float3(simplePerlin2D7 , 0.0 , simplePerlin3D8));
			float clampResult42 = clamp( ase_vertex3Pos.y , 0.0 , 100000.0 );
			v.vertex.xyz += ( float4( ( ( _FlutterAmount * appendResult67 ) + ( _MainWaveAmount * float3( 0.01,0.01,0.01 ) * appendResult17 * clampResult42 ) ) , 0.0 ) * v.color ).rgb;
			v.vertex.w = 1;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 lerpResult44 = lerp( _MainColor , _TopColor , saturate( ( ( ase_vertex3Pos.y - _GradientOffset1 ) * _GradientSmoothness1 ) ));
			o.Albedo = lerpResult44.rgb;
			o.Emission = ( lerpResult44 * _Emmision ).rgb;
			o.Alpha = saturate( ( ( 1.0 - ase_vertex3Pos.y ) + (-2.0 + (_opacityValue - 0.0) * (10.0 - -2.0) / (1.0 - 0.0)) ) );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
}
/*ASEBEGIN
Version=18912
46;111;1753;641;2828.309;242.9121;1.759547;True;True
Node;AmplifyShaderEditor.SimpleTimeNode;57;-2994.855,645.5877;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;13;-3109.338,750.3044;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-3011.208,1169.172;Inherit;False;Property;_WaveSpeed;Wave Speed;4;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;72;-3171.114,903.0013;Inherit;False;Property;_WaveHeightMultiplier;Wave Height Multiplier;7;0;Create;True;0;0;0;False;0;False;1;0.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;9;-3000.498,1272.358;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;-2989.565,546.4023;Inherit;False;Property;_FlutterSpeed;Flutter Speed;10;0;Create;True;0;0;0;False;0;False;0.5;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;52;-3108.953,989.6024;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-2799.546,572.3633;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;-2890.315,884.8015;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-2778.674,1432.801;Inherit;False;Property;_MainWaveScale;Main Wave Scale;5;0;Create;True;0;0;0;False;0;False;1;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-2805.188,1199.133;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-2708.911,707.3857;Inherit;False;Property;_FlutterScale;Flutter Scale;11;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;38;-2654.321,1163.21;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;63;-2648.678,536.4399;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;74;-2864.487,327.8;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleDivideOpNode;16;-2478.8,1282.066;Inherit;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;62;-2473.157,655.2965;Inherit;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;51;-2690.37,953.1699;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;80;-1096.359,-677.9014;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;65;-2249.411,665.4032;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;81;-1084.935,-498.8184;Inherit;False;Property;_GradientOffset1;Gradient Offset;9;0;Create;True;0;0;0;False;0;False;0;3.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-2255.053,1292.173;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.8;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-2471.037,1079.125;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;64;-2465.394,452.3552;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;7;-2034.295,956.4782;Inherit;True;Simplex2D;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;69;-2028.695,586.5114;Inherit;True;Simplex3D;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-932.7335,-374.9674;Inherit;False;Property;_GradientSmoothness1;Gradient Smoothness;6;0;Create;True;0;0;0;False;0;False;0.5;0.22;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;8;-2034.338,1213.281;Inherit;True;Simplex3D;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;83;-866.7264,-562.5815;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;40;-2000.669,1440.658;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;70;-2027.352,329.7086;Inherit;True;Simplex3D;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-1722.875,360.4977;Inherit;False;Property;_FlutterAmount;Flutter Amount;8;0;Create;True;0;0;0;False;0;False;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-663.0674,-516.7563;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;92;-1590.783,-17.32623;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;97;-1627.413,147.0328;Inherit;False;Property;_opacityValue;opacityValue;12;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;42;-1675.566,1358.895;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;100000;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-1728.518,987.2673;Inherit;False;Property;_MainWaveAmount;Main Wave Amount;3;0;Create;True;0;0;0;False;0;False;0.3;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;67;-1695.654,478.0676;Inherit;True;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;17;-1701.297,1104.837;Inherit;True;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TFHCRemapNode;96;-1336.413,221.0328;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-2;False;4;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;88;-1335.925,25.74728;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1;-651.0666,-905.695;Inherit;False;Property;_MainColor;Main Color;1;1;[HDR];Create;True;0;0;0;False;0;False;0.3160377,1,0.695684,1;0.8784314,0.3764706,0.542779,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;75;-384.6093,-547.7219;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-1459.116,1072.95;Inherit;True;4;4;0;FLOAT;0;False;1;FLOAT3;0.01,0.01,0.01;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;68;-1453.473,446.1802;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;43;-650.6108,-727.0948;Inherit;False;Property;_TopColor;Top Color;0;1;[HDR];Create;True;0;0;0;False;0;False;0.3160377,1,0.695684,1;1,0.6078432,0.6705946,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;44;-307.7633,-744.3168;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;87;-447.6089,917.0267;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;19;-721.1369,867.4346;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;90;-1053.206,28.89307;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;2;-494.422,-398.4309;Inherit;False;Property;_Emmision;Emmision;2;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;85;-204.0244,-303.741;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;91;-760.202,142.6319;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;-249.1189,788.9595;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;8.799211,7.152557E-07;Float;False;True;-1;2;;0;0;Standard;Distant Lands/Stylized Dynamic Coral;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;59;0;56;0
WireConnection;59;1;57;0
WireConnection;73;0;13;2
WireConnection;73;1;72;0
WireConnection;36;0;11;0
WireConnection;36;1;9;0
WireConnection;38;0;36;0
WireConnection;38;1;36;0
WireConnection;38;2;36;0
WireConnection;63;0;59;0
WireConnection;63;1;59;0
WireConnection;63;2;59;0
WireConnection;16;1;14;0
WireConnection;62;1;71;0
WireConnection;51;0;52;1
WireConnection;51;1;73;0
WireConnection;51;2;52;3
WireConnection;65;0;62;0
WireConnection;15;0;16;0
WireConnection;39;0;51;0
WireConnection;39;1;38;0
WireConnection;64;0;74;0
WireConnection;64;1;63;0
WireConnection;7;0;39;0
WireConnection;7;1;16;0
WireConnection;69;0;64;0
WireConnection;69;1;65;0
WireConnection;8;0;39;0
WireConnection;8;1;15;0
WireConnection;83;0;80;2
WireConnection;83;1;81;0
WireConnection;70;0;64;0
WireConnection;70;1;62;0
WireConnection;84;0;83;0
WireConnection;84;1;82;0
WireConnection;42;0;40;2
WireConnection;67;0;70;0
WireConnection;67;2;69;0
WireConnection;17;0;7;0
WireConnection;17;2;8;0
WireConnection;96;0;97;0
WireConnection;88;0;92;2
WireConnection;75;0;84;0
WireConnection;18;0;4;0
WireConnection;18;2;17;0
WireConnection;18;3;42;0
WireConnection;68;0;66;0
WireConnection;68;1;67;0
WireConnection;44;0;1;0
WireConnection;44;1;43;0
WireConnection;44;2;75;0
WireConnection;19;0;68;0
WireConnection;19;1;18;0
WireConnection;90;0;88;0
WireConnection;90;1;96;0
WireConnection;85;0;44;0
WireConnection;85;1;2;0
WireConnection;91;0;90;0
WireConnection;86;0;19;0
WireConnection;86;1;87;0
WireConnection;0;0;44;0
WireConnection;0;2;85;0
WireConnection;0;9;91;0
WireConnection;0;11;86;0
ASEEND*/
//CHKSM=2C9844F4D9EE304CE983F5444DDF6C1CD28E0D59