// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DirtyShader"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_DissolveAlbedo("Dissolve Albedo", 2D) = "white" {}
		_Normal("Normal", 2D) = "white" {}
		_DissolveNormal("Dissolve Normal", 2D) = "white" {}
		_DissolveEdgeStrenght("Dissolve Edge Strenght", Float) = 0.01
		_DissolveStrenght("Dissolve Strenght", Float) = -0.3
		_NoiseStrenght("Noise Strenght", Float) = 50
		_AlbedoColor("Albedo Color", Color) = (1,1,1,0)
		_DissolveColor("Dissolve Color", Color) = (1,1,1,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		float4 _Normal_TexelSize;
		uniform half _NoiseStrenght;
		uniform half _DissolveEdgeStrenght;
		uniform half _DissolveStrenght;
		uniform half4 _AlbedoColor;
		uniform sampler2D _DissolveNormal;
		uniform float4 _DissolveNormal_ST;
		float4 _DissolveNormal_TexelSize;
		uniform half4 _DissolveColor;
		uniform sampler2D _Albedo;
		uniform half4 _Albedo_ST;
		uniform sampler2D _DissolveAlbedo;
		uniform half4 _DissolveAlbedo_ST;


		inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }

		inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }

		inline float valueNoise (float2 uv)
		{
			float2 i = floor(uv);
			float2 f = frac( uv );
			f = f* f * (3.0 - 2.0 * f);
			uv = abs( frac(uv) - 0.5);
			float2 c0 = i + float2( 0.0, 0.0 );
			float2 c1 = i + float2( 1.0, 0.0 );
			float2 c2 = i + float2( 0.0, 1.0 );
			float2 c3 = i + float2( 1.0, 1.0 );
			float r0 = noise_randomValue( c0 );
			float r1 = noise_randomValue( c1 );
			float r2 = noise_randomValue( c2 );
			float r3 = noise_randomValue( c3 );
			float bottomOfGrid = noise_interpolate( r0, r1, f.x );
			float topOfGrid = noise_interpolate( r2, r3, f.x );
			float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
			return t;
		}


		float SimpleNoise(float2 UV)
		{
			float t = 0.0;
			float freq = pow( 2.0, float( 0 ) );
			float amp = pow( 0.5, float( 3 - 0 ) );
			t += valueNoise( UV/freq )*amp;
			freq = pow(2.0, float(1));
			amp = pow(0.5, float(3-1));
			t += valueNoise( UV/freq )*amp;
			freq = pow(2.0, float(2));
			amp = pow(0.5, float(3-2));
			t += valueNoise( UV/freq )*amp;
			return t;
		}


		half3 CombineSamplesSharp128_g3( half S0, half S1, half S2, half Strength )
		{
			{
			    float3 va = float3( 0.13, 0, ( S1 - S0 ) * Strength );
			    float3 vb = float3( 0, 0.13, ( S2 - S0 ) * Strength );
			    return normalize( cross( va, vb ) );
			}
		}


		half3 CombineSamplesSharp128_g4( half S0, half S1, half S2, half Strength )
		{
			{
			    float3 va = float3( 0.13, 0, ( S1 - S0 ) * Strength );
			    float3 vb = float3( 0, 0.13, ( S2 - S0 ) * Strength );
			    return normalize( cross( va, vb ) );
			}
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			half localCalculateUVsSharp110_g3 = ( 0.0 );
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			half2 temp_output_85_0_g3 = uv_Normal;
			half2 UV110_g3 = temp_output_85_0_g3;
			half4 TexelSize110_g3 = _Normal_TexelSize;
			half2 UV0110_g3 = float2( 0,0 );
			half2 UV1110_g3 = float2( 0,0 );
			half2 UV2110_g3 = float2( 0,0 );
			{
			{
			    UV110_g3.y -= TexelSize110_g3.y * 0.5;
			    UV0110_g3 = UV110_g3;
			    UV1110_g3 = UV110_g3 + float2( TexelSize110_g3.x, 0 );
			    UV2110_g3 = UV110_g3 + float2( 0, TexelSize110_g3.y );
			}
			}
			half4 break134_g3 = tex2D( _Normal, UV0110_g3 );
			half S0128_g3 = break134_g3.r;
			half4 break136_g3 = tex2D( _Normal, UV1110_g3 );
			half S1128_g3 = break136_g3.r;
			half4 break138_g3 = tex2D( _Normal, UV2110_g3 );
			half S2128_g3 = break138_g3.r;
			half2 temp_cast_0 = (_NoiseStrenght).xx;
			float2 uv_TexCoord15 = i.uv_texcoord * temp_cast_0;
			half simpleNoise8 = SimpleNoise( uv_TexCoord15 );
			half temp_output_6_0 = step( simpleNoise8 , ( _DissolveEdgeStrenght + (0.0 + (_DissolveStrenght - -1.0) * (1.0 - 0.0) / (1.0 - -1.0)) ) );
			half temp_output_91_0_g3 = temp_output_6_0;
			half Strength128_g3 = temp_output_91_0_g3;
			half3 localCombineSamplesSharp128_g3 = CombineSamplesSharp128_g3( S0128_g3 , S1128_g3 , S2128_g3 , Strength128_g3 );
			half localCalculateUVsSharp110_g4 = ( 0.0 );
			float2 uv_DissolveNormal = i.uv_texcoord * _DissolveNormal_ST.xy + _DissolveNormal_ST.zw;
			half2 temp_output_85_0_g4 = uv_DissolveNormal;
			half2 UV110_g4 = temp_output_85_0_g4;
			half4 TexelSize110_g4 = _DissolveNormal_TexelSize;
			half2 UV0110_g4 = float2( 0,0 );
			half2 UV1110_g4 = float2( 0,0 );
			half2 UV2110_g4 = float2( 0,0 );
			{
			{
			    UV110_g4.y -= TexelSize110_g4.y * 0.5;
			    UV0110_g4 = UV110_g4;
			    UV1110_g4 = UV110_g4 + float2( TexelSize110_g4.x, 0 );
			    UV2110_g4 = UV110_g4 + float2( 0, TexelSize110_g4.y );
			}
			}
			half4 break134_g4 = tex2D( _DissolveNormal, UV0110_g4 );
			half S0128_g4 = break134_g4.r;
			half4 break136_g4 = tex2D( _DissolveNormal, UV1110_g4 );
			half S1128_g4 = break136_g4.r;
			half4 break138_g4 = tex2D( _DissolveNormal, UV2110_g4 );
			half S2128_g4 = break138_g4.r;
			half temp_output_91_0_g4 = ( 1.0 - temp_output_6_0 );
			half Strength128_g4 = temp_output_91_0_g4;
			half3 localCombineSamplesSharp128_g4 = CombineSamplesSharp128_g4( S0128_g4 , S1128_g4 , S2128_g4 , Strength128_g4 );
			o.Normal = BlendNormals( ( half4( localCombineSamplesSharp128_g3 , 0.0 ) * _AlbedoColor ).rgb , ( half4( localCombineSamplesSharp128_g4 , 0.0 ) * _DissolveColor ).rgb );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float2 uv_DissolveAlbedo = i.uv_texcoord * _DissolveAlbedo_ST.xy + _DissolveAlbedo_ST.zw;
			o.Albedo = ( ( temp_output_6_0 * ( tex2D( _Albedo, uv_Albedo ) * _AlbedoColor ) ) + ( ( 1.0 - temp_output_6_0 ) * ( tex2D( _DissolveAlbedo, uv_DissolveAlbedo ) * _DissolveColor ) ) ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
122.4;174.4;1191.2;599.8;3851.518;640.3922;5.027957;True;False
Node;AmplifyShaderEditor.CommentaryNode;19;-1149.116,-338.817;Inherit;False;1034.351;623.6353;Create the noise and modify his parameters;8;17;2;3;5;15;4;8;6;Noise Dissolve;0,0.9898858,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-1024,48;Inherit;False;Property;_DissolveStrenght;Dissolve Strenght;5;0;Create;True;0;0;0;False;0;False;-0.3;-0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1099.116,-270.2779;Inherit;False;Property;_NoiseStrenght;Noise Strenght;6;0;Create;True;0;0;0;False;0;False;50;50;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;3;-786.7287,19.152;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-867.9126,-287.0514;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-833.4942,-99.11063;Inherit;False;Property;_DissolveEdgeStrenght;Dissolve Edge Strenght;4;0;Create;True;0;0;0;False;0;False;0.01;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;60;-1122,542;Inherit;False;1250.802;867;Put the Albedo textures (Add a different Albedo texture if you need the noise have it, else select a color to the noise) ;12;58;59;22;28;29;20;24;31;27;30;25;32;Noise + Object Textures;1,0,0,1;0;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;8;-600.9462,-288.817;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;4;-568.5446,28.3034;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;66;-2578,670;Inherit;False;1047.2;696;Put the Normal textures (Add a different Normal texture if you need the noise have it) ;8;62;61;64;57;56;51;52;65;Normal Tex´s;0.06203568,1,0,1;0;0
Node;AmplifyShaderEditor.TexturePropertyNode;58;-1072,592;Inherit;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.TexturePropertyNode;59;-1072,992;Inherit;True;Property;_DissolveAlbedo;Dissolve Albedo;1;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.StepOpNode;6;-350.165,31.41834;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;20;-816,592;Inherit;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;29;-736,1200;Inherit;False;Property;_DissolveColor;Dissolve Color;8;0;Create;True;0;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;22;-816,992;Inherit;True;Property;_TextureSample1;Texture Sample 1;6;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;62;-2528,1136;Inherit;True;Property;_DissolveNormal;Dissolve Normal;3;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.TexturePropertyNode;61;-2528,720;Inherit;True;Property;_Normal;Normal;2;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.OneMinusNode;64;-2144,960;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;28;-736,784;Inherit;False;Property;_AlbedoColor;Albedo Color;7;0;Create;True;0;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-464,1024;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;31;-368,832;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-464,640;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;57;-2272,1136;Inherit;False;Normal From Texture;-1;;4;9728ee98a55193249b513caf9a0f1676;13,149,0,147,0,143,0,141,0,139,0,151,0,137,0,153,0,159,0,157,0,155,0,135,0,108,0;4;87;SAMPLER2D;0;False;85;FLOAT2;0,0;False;74;SAMPLERSTATE;0;False;91;FLOAT;1;False;2;FLOAT3;40;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;56;-2272,720;Inherit;False;Normal From Texture;-1;;3;9728ee98a55193249b513caf9a0f1676;13,149,0,147,0,143,0,141,0,139,0,151,0,137,0,153,0,159,0,157,0,155,0,135,0,108,0;4;87;SAMPLER2D;0;False;85;FLOAT2;0,0;False;74;SAMPLERSTATE;0;False;91;FLOAT;1;False;2;FLOAT3;40;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-240,640;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-240,1024;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1920,768;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-1920,1152;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendNormalsNode;65;-1760,960;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-23.59766,822.3738;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;256,784;Half;False;True;-1;2;ASEMaterialInspector;0;0;Standard;DirtyShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;2;0
WireConnection;15;0;17;0
WireConnection;8;0;15;0
WireConnection;4;0;5;0
WireConnection;4;1;3;0
WireConnection;6;0;8;0
WireConnection;6;1;4;0
WireConnection;20;0;58;0
WireConnection;22;0;59;0
WireConnection;64;0;6;0
WireConnection;27;0;22;0
WireConnection;27;1;29;0
WireConnection;31;0;6;0
WireConnection;24;0;20;0
WireConnection;24;1;28;0
WireConnection;57;87;62;0
WireConnection;57;91;64;0
WireConnection;56;87;61;0
WireConnection;56;91;6;0
WireConnection;25;0;6;0
WireConnection;25;1;24;0
WireConnection;30;0;31;0
WireConnection;30;1;27;0
WireConnection;52;0;56;40
WireConnection;52;1;28;0
WireConnection;51;0;57;40
WireConnection;51;1;29;0
WireConnection;65;0;52;0
WireConnection;65;1;51;0
WireConnection;32;0;25;0
WireConnection;32;1;30;0
WireConnection;0;0;32;0
WireConnection;0;1;65;0
ASEEND*/
//CHKSM=7072598AAC933308F9BD8A3BCBC8441651EDD28B