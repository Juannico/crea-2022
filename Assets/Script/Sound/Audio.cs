using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Audio : MonoBehaviour
{
    public AudioMixerSnapshot Paused;
    public AudioMixerSnapshot Unpaused;
    public void TransitionToPause()
    {
        Paused.TransitionTo(.01f);
    }

    public void TransitionToUnpaused()
    {
        Unpaused.TransitionTo(.01f);
    }
}
