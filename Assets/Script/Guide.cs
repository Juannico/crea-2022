using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide : MonoBehaviour
{
    [SerializeField] private GameObject guidePrefab;
    private bool canCreate;
    private List<GameObject> guidePool = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        guidePool.Add(guidePrefab);
        StartCoroutine(CreateGuide());
        canCreate = true;
    }

    // Update is called once per frame
    void Update()
    {
    }
    // crea una guia cada X tiempo , si la primera creada se encuentra activa
    private IEnumerator CreateGuide()
    {

        yield return new WaitForSeconds(4.5f); // 
        GameObject guideInstace;
        if (guidePool[0].activeInHierarchy)
        {
            guideInstace = Instantiate(guidePrefab, Vector3.zero, Quaternion.identity, this.transform);
            guidePool.Add(guideInstace);
        }
        else
        {
            guidePool[0].SetActive(true);
        }
        if (canCreate) StartCoroutine(CreateGuide());
    }


}
