using Cinemachine;
using System;
using UnityEngine;

public class ComplexCinematic : MonoBehaviour
{
    [SerializeField] private float cinematicDuration;
    [SerializeField] private CinemachineVirtualCamera cinematicCamera;
    public Action OnCinematicFinished;

    public void ActivateCinematic()
    {
        if (cinematicCamera == null) return;
        cinematicCamera.Priority = 10;
        StartCoroutine(CoroutineHandler.ExecuteActionAfter(() =>
        {
            OnCinematicFinished?.Invoke();
        }, cinematicDuration));
    }
}
