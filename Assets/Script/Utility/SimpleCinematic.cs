using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCinematic : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera cinematicCamera;
    [SerializeField] private float cinematicDuration;

    public void ActivateCinematic()
    {
        if (cinematicCamera == null) return;
        cinematicCamera.Priority = 10;
        StartCoroutine(CoroutineHandler.ExecuteActionAfter(() =>
        {
            cinematicCamera.Priority = 0;
        }, cinematicDuration));
    }
}
