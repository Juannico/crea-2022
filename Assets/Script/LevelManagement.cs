using System.Collections.Generic;
using UnityEngine;

public class LevelManagement : MonoBehaviour
{
    private GameManager gameManager;
    private UIManager UIManager;
    [SerializeField] private Guide guide;
    [SerializeField] private GameObject trash;
    [SerializeField] private GameObject flockGroup;
    [SerializeField] private GameObject cameraCinemactic;
    private List<Flock> flock;
    private int flockIndex = 0;
    [Range(0, 100)] [SerializeField] private int percentageNeededToUnlonck;
    public GameObject limitZone;

    public float totalTrashValue { get => setTrashValue(); }
    private float _levelTrashPercentage = 100;
    public float levelTrashPercentage { get => _levelTrashPercentage; }

    [SerializeField] private int levelIndex;

    private void Awake()
    {
        //getFlocks();
    }

    private void Start()
    {
        cameraCinemactic.SetActive(false);
        gameManager = FindObjectOfType<GameManager>();
        UIManager = FindObjectOfType<UIManager>();
       
    }
    void Update()
    {
    }

    //Crea una lista de la cantida de cardumenes que hay en el nivel y los oculta
    private void getFlocks()
    {
        flock = new List<Flock>(flockGroup.GetComponentsInChildren<Flock>());
        foreach (Flock item in flock)
        {
            item.gameObject.SetActive(false);
        }
    }
    // Toma todos los objetos considerados con el script Dirty y calcula la cantidad total de "basura" dentro del nivel 
    private float setTrashValue()
    {
        float levelTrashValue = 0;
        List<Transform> trashObjects = new List<Transform>(trash.GetComponentsInChildren<Transform>());
        trashObjects.RemoveAt(0);
        foreach (Transform item in trashObjects)
        {
            float currentDirtyValue = 0;
            if (item.GetComponent<Dirty>()) currentDirtyValue = item.GetComponent<Dirty>().trashValue;
            levelTrashValue += currentDirtyValue;
        }
        return levelTrashValue;
    }
    // Actualiza el porcentage de basura deacuerdo a la cantidad de "basura" limipada
    public void SetTrashPercentage(float percentageValue)
    {

        _levelTrashPercentage -= percentageValue;
        if (_levelTrashPercentage > 100) _levelTrashPercentage = 100;
        if (_levelTrashPercentage < 0) _levelTrashPercentage = 0;
        //StartCoroutine(UIManager.UpdateCleanPercetageBarAnim(percentageValue));
    }

    public void ShowFlock()
    {
        if (flockIndex < flock.Count)
        {
            flock[flockIndex].gameObject.SetActive(true);
            flockIndex++;
        }
    }
    // Muestra todos los cardumenes que estan dentro del nivel que estan ocultos
    public void ShowAllFlocks()
    {
        foreach (Flock item in flock)
            if (!item.gameObject.activeInHierarchy)
                item.gameObject.SetActive(true);
    }
    // Actualiza el nivel 
    public void setLevel()
    {
        gameManager.CurrentLevel(levelIndex);
        StartCoroutine(UIManager.UpdateCleanPercetageBarAnim(0));
    }
    // Activa la cinematica y "limpia"/"revive" todos los objetos restantes dentro del nivel 
    public void cleanDirty()
    {
        cameraCinemactic.SetActive(true);
        List<Dirty> trashObjects = new List<Dirty>(trash.GetComponentsInChildren<Dirty>());
        List<Revive> diedEnviroments = new List<Revive>(trash.GetComponentsInChildren<Revive>());
        foreach (Dirty item in trashObjects)
        {
            item.CleanDirty();
        }
        foreach (Revive item in diedEnviroments) {
            item.ReviveEnviroment();
        }
    }
}
