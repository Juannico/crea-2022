using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public GameObject Tutorial_canvas, Imagen;
    public GameObject[] Instructions;
    private int Instructions_index;
    private bool end;

    private void Start()
    {
        Tutorial_canvas.SetActive(true);
        Instructions[Instructions_index].SetActive(true);
    }

    public void NextTutorial()
    {
        if (end) return;
        Instructions[Instructions_index].SetActive(false);
        if (Instructions_index + 1 > Instructions.Length - 1)
        {
            End();
        }
        else
            Instructions[++Instructions_index].SetActive(true);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            NextTutorial();
        }
    }
    public void End()
    {
        Tutorial_canvas.SetActive(false);
        end = true;
    }
}
