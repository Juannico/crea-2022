using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Vector2 CamaraEjes;
    [SerializeField] private float sensibilidad = .5f, vel = 0.1f, limitemaxy = 60, limiteminy = -60, Aceleracion = 5f;
    private Vector3 movement;
    public Camera camara;
    public GameObject jugador;
    [SerializeField]
    Rigidbody rb;

    public float Sensibilidad { get => sensibilidad; set => sensibilidad = value; }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        sensibilidad = PlayerPrefs.GetFloat("Sensitivity") == 0 ? 1 : PlayerPrefs.GetFloat("Sensitivity");
    }


    private void FixedUpdate()
    {
        cam();

        if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space))// barra o click izquierdo para activar movimiento
        {
            movimiento();
        }

        if (Input.GetMouseButton(1))
        {
            aceleracion();
        }
    }

    public void cam()//rotacion de la camara Mouse
    {
        CamaraEjes.x += Input.GetAxis("Mouse X") * sensibilidad;
        CamaraEjes.y += Input.GetAxis("Mouse Y") * sensibilidad;
        CamaraEjes.y = Mathf.Clamp(CamaraEjes.y, limiteminy, limitemaxy);
        transform.localRotation = Quaternion.Euler(-CamaraEjes.y, CamaraEjes.x, 0);
    }

    public void movimiento() // realiza los calculos para el movimiento
    {
        Ray raycats = camara.ScreenPointToRay(Input.mousePosition);
        float Distancia = vel * 4 * Time.deltaTime;//movimiento eje 
        rb.AddForce(raycats.direction * Distancia);
    }

    void aceleracion()
    {
        Ray raycats = camara.ScreenPointToRay(Input.mousePosition);
        float boost = vel * Aceleracion;
        float Distancia = boost * 2 * Time.deltaTime;//movimiento eje 
        rb.AddForce(raycats.direction * Distancia);
    }

}
