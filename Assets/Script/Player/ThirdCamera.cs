using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdCamera : MonoBehaviour
{
    [SerializeField] private Transform target;
    //[HideInInspector]private float mouseX, mouseY;
    private float _mouseX;
    public float mouseX { get => _mouseX; }
    public float mouseY { get; set; }
    private Vector2 mouseSensibility;
    // Start is called before the first frame update
    void Start()
    {
        mouseSensibility.x = 1;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        _mouseX += Input.GetAxis("Mouse X") * mouseSensibility.x;
        mouseY += Input.GetAxis("Mouse Y");
        transform.LookAt(target);
        target.rotation = Quaternion.Euler(/*mouseY */0, mouseX , 0);
    }
}
