using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Revive : MonoBehaviour
{
    [SerializeField] private float reviveSpeed = 1;
    private bool isDead;
    private Material material;

    // Start is called before the first frame update
    void Start()
    {

        isDead = true;
        material = GetComponent<MeshRenderer>().material;
        //Oculta el objeto 
        material.SetFloat("_opacityValue", 0);
    }

    // Update is called once per frame

    public void ReviveEnviroment()
    {
        if (isDead)
        {
            isDead = false;
            StartCoroutine(ReviveEnviromentAnim());

        }
    }

    // Transicion para mostrar el objeto 
    private IEnumerator ReviveEnviromentAnim()
    {
        yield return new WaitForSeconds(0.5f);
        float opacityValue = material.GetFloat("_opacityValue");
        while (opacityValue < 1)
        {
            opacityValue += reviveSpeed * Time.deltaTime;
            material.SetFloat("_opacityValue", opacityValue);
            yield return null;
        }
        material.SetFloat("_opacityValue", 1);
        yield return null;
    }
}
