using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableArtifact : MonoBehaviour
{
    [SerializeField] private GameObject explosionParticleSystem;
    [SerializeField] private ParticleSystem[] lingeringParticleSystem;

    private Renderer itemMesh;

    public delegate void OnItemTouched();
    public OnItemTouched onItemTouched;
    // Start is called before the first frame update
    void Start()
    {
        itemMesh = GetComponentInChildren<Renderer>();
        onItemTouched += () =>
        {
            itemMesh.enabled = false;
            Instantiate(explosionParticleSystem,transform.position,transform.rotation).GetComponent<ParticleSystem>().Play();
            foreach (var item in lingeringParticleSystem)
            {
                item.Stop();
            }
        };
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            onItemTouched.Invoke();
        }
    }
}
