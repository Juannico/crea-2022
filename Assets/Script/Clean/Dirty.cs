using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UIElements;

public class Dirty : MonoBehaviour
{

    private bool isDirty;
    private Material dirtyMat;
    [SerializeField] private int _trashValue;
    [SerializeField] float FadeVelocity = -0.1f;
    [SerializeField] float FadeLimit = -0.5f;
    [SerializeField] GameObject[] dirtyObjects;
    [SerializeField] GameObject ParticleObject;
    [Header("Cinematic")]
    [SerializeField] private SimpleCinematic cinematic;
    [Header("Sound")]
    [SerializeField] private AudioClip cleanSound;
    public int trashValue { get => _trashValue; }
    public bool IsDirty { get => isDirty; set => isDirty = value; }

    private float trashPercentage;
    private Vector3 escala = new Vector3(0.1f, 0.1f, 0.1f);

    private bool callOnce = true;

    [SerializeField] public List<GameObject> lista_objetivos = new List<GameObject>();

    private LevelManagement levelMangent;
    void Start()
    {
        levelMangent = GetComponentInParent<LevelManagement>();
        trashPercentage = getTrashPercentage();
        isDirty = true;
        dirtyMat = GetComponent<MeshRenderer>().material;
        foreach (var item in lista_objetivos)
        {
            PickableArtifact pickableArtifact = item.GetComponent<PickableArtifact>();
            pickableArtifact.onItemTouched += () =>
            {
                lista_objetivos.Remove(item);
            };
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (lista_objetivos.Count == 0)
        {
            CleanDirty();
            ParticleObject.SetActive(true);
        }
    }
    // limipia el objeto y llama la funcion que actuliza el porcentaje de "basura"
    public void CleanDirty()
    {
        if (isDirty)
        {
            isDirty = Efecto();
            FindObjectOfType<GameManager>().CheckIfDone();
            if (cleanSound != null)
            {
                gameObject.AddComponent<AudioSource>().PlayOneShot(cleanSound);
            }
            levelMangent.SetTrashPercentage(trashPercentage);
        }
    }
    // limipia el objeto y llama la funcion que actuliza el porcentaje de "basura"
    public void CleanTrash(GameObject dirtyObject)
    {
        float globalReduction = 0;
        var DirtyRenderer = dirtyObject.GetComponent<Renderer>();
        if (DirtyRenderer.material.shader.name.Contains("Dirty"))
        {
            StartCoroutine(CoroutineHandler.UpdateUntil(() =>
            {
                var reduction = Mathf.MoveTowards(DirtyRenderer.material.GetFloat("_DissolveStrenght"), FadeLimit, FadeVelocity * Time.deltaTime);
                DirtyRenderer.material.SetFloat("_DissolveStrenght", reduction);
                globalReduction += FadeVelocity * Time.deltaTime;
                print("pass");

            }, globalReduction >= FadeLimit));
        }
    }
    // Crea el porcentage de basura conrrespondiente al objeto de acuerdo al nivel
    private float getTrashPercentage()
    {
        float currentTrashPrecentage;
        currentTrashPrecentage = (_trashValue * 100) / levelMangent.totalTrashValue;
        return currentTrashPrecentage;
    }

    public bool Efecto()
    {
        foreach (var item in dirtyObjects)
        {
            var ReviveScript = item.GetComponent<Revive>();
            CleanTrash(item);
            if (ReviveScript != null)
            {
                ReviveScript.ReviveEnviroment();
            }
        }

        cinematic.ActivateCinematic();
        callOnce = false;
        // Debug.Log( DirtyRenderer.material.GetFloat("_DissolveStrenght"));
        return callOnce;
    }
}
