using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFog : MonoBehaviour
{
    [SerializeField] private Material dirtyMaterialReference;
    [SerializeField] private Material cleanMaterialReference;
    private Material objectMaterial;

    [Header("Interpolation Value")]
    [Range(0.0f,1.0f)]
    public float fogInterpolationValue;
    // Start is called before the first frame update
    void Start()
    {
        objectMaterial = GetComponentInChildren<MeshRenderer>().material;
    }

    private void Update()
    {
        SetMaterialProperties();
    }

    public void SetMaterialProperties()
    {
        //Colors
        objectMaterial.SetColor("_FogColor1", Color.Lerp(dirtyMaterialReference.GetColor("_FogColor1"),cleanMaterialReference.GetColor("_FogColor1"),fogInterpolationValue));
        objectMaterial.SetColor("_FogColor2", Color.Lerp(dirtyMaterialReference.GetColor("_FogColor2"), cleanMaterialReference.GetColor("_FogColor2"), fogInterpolationValue));
        objectMaterial.SetColor("_FogColor3", Color.Lerp(dirtyMaterialReference.GetColor("_FogColor3"), cleanMaterialReference.GetColor("_FogColor3"), fogInterpolationValue));
        objectMaterial.SetColor("_FogColor4", Color.Lerp(dirtyMaterialReference.GetColor("_FogColor4"), cleanMaterialReference.GetColor("_FogColor4"), fogInterpolationValue));
        objectMaterial.SetColor("_FogColor5", Color.Lerp(dirtyMaterialReference.GetColor("_FogColor5"), cleanMaterialReference.GetColor("_FogColor5"), fogInterpolationValue));

        //Color Start
        objectMaterial.SetFloat("_ColorStart1", Mathf.Lerp(dirtyMaterialReference.GetFloat("_ColorStart1"), cleanMaterialReference.GetFloat("_ColorStart1"), fogInterpolationValue));
        objectMaterial.SetFloat("_ColorStart2", Mathf.Lerp(dirtyMaterialReference.GetFloat("_ColorStart2"), cleanMaterialReference.GetFloat("_ColorStart2"), fogInterpolationValue));
        objectMaterial.SetFloat("_ColorStart3", Mathf.Lerp(dirtyMaterialReference.GetFloat("_ColorStart3"), cleanMaterialReference.GetFloat("_ColorStart3"), fogInterpolationValue));
        objectMaterial.SetFloat("_ColorStart4", Mathf.Lerp(dirtyMaterialReference.GetFloat("_ColorStart4"), cleanMaterialReference.GetFloat("_ColorStart4"), fogInterpolationValue));

        //The rest
        objectMaterial.SetFloat("_FogDepthMultiplier", Mathf.Lerp(dirtyMaterialReference.GetFloat("_FogDepthMultiplier"), cleanMaterialReference.GetFloat("_FogDepthMultiplier"), fogInterpolationValue));
        objectMaterial.SetFloat("_LightFalloff", Mathf.Lerp(dirtyMaterialReference.GetFloat("_LightFalloff"), cleanMaterialReference.GetFloat("_LightFalloff"), fogInterpolationValue));
        objectMaterial.SetFloat("LightIntensity", Mathf.Lerp(dirtyMaterialReference.GetFloat("LightIntensity"), cleanMaterialReference.GetFloat("LightIntensity"), fogInterpolationValue));
        objectMaterial.SetFloat("_FogSmoothness", Mathf.Lerp(dirtyMaterialReference.GetFloat("_FogSmoothness"), cleanMaterialReference.GetFloat("_FogSmoothness"), fogInterpolationValue));        
        objectMaterial.SetFloat("_FogIntensity", Mathf.Lerp(dirtyMaterialReference.GetFloat("_FogIntensity"), cleanMaterialReference.GetFloat("_FogIntensity"), fogInterpolationValue));
        objectMaterial.SetFloat("_FogOffset", Mathf.Lerp(dirtyMaterialReference.GetFloat("_FogOffset"), cleanMaterialReference.GetFloat("_FogOffset"), fogInterpolationValue));

        objectMaterial.SetColor("_LightColor", Color.Lerp(dirtyMaterialReference.GetColor("_LightColor"), cleanMaterialReference.GetColor("_LightColor"), fogInterpolationValue));
    }

}
