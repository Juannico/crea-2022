using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuButton : Selectable
{
    public GameObject SelectedArrowImage;
    public UnityEvent onSubmit;

    protected override void Awake()
    {
        base.Awake();
        SelectedArrowImage.SetActive(false);
    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        SelectedArrowImage.SetActive(true);
        //TODO Also change the font type to bold
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        SelectedArrowImage.SetActive(false);
        //TODO Also change the font type to regular
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        Select();
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        if(eventData.pressPosition.Equals(eventData.position))
            onSubmit?.Invoke();
    }
}
