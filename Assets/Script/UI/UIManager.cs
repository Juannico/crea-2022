using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private RectTransform cleanPercentageBar;
    private GameManager gameManager;
    [SerializeField] private GameObject alertText;
    
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        alertText.SetActive(false);

        cleanPercentageBar.transform.localScale = new Vector3(0, cleanPercentageBar.localScale.y, cleanPercentageBar.localScale.z);
    }
    // Muestra un mensaje en la pantalla
    public void StartAlert(string message, float time)
    {
        if (!alertText.activeSelf)
        {
            alertText.GetComponent<Text>().text = message;
            alertText.SetActive(true);
            StartCoroutine(AlertAnim(time));
        }
    }
    // Apaga el mensaje
    private IEnumerator AlertAnim(float time)
    {
        yield return new WaitForSeconds(time);
        alertText.SetActive(false);
    }

    // Actuliza en la UI el porcentage de "basura"
    public IEnumerator UpdateCleanPercetageBarAnim(float trashvalue)
    {
        while (cleanPercentageBar.transform.localScale.x < 100 - gameManager.currentLevel.levelTrashPercentage)
        {
            cleanPercentageBar.transform.localScale = new Vector3(cleanPercentageBar.transform.localScale.x + (Time.deltaTime * trashvalue * 0.9f), cleanPercentageBar.localScale.y, cleanPercentageBar.localScale.z);
            yield return null;
        }
        cleanPercentageBar.transform.localScale = new Vector3(100 - gameManager.currentLevel.levelTrashPercentage, cleanPercentageBar.localScale.y, cleanPercentageBar.localScale.z);
    }
}
