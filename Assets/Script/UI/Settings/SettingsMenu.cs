using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private SettingBar[] settingsBars;
    private float repeatDelay;
    private float lastUpdate;

    private int currentSettingIndex = 0;

    public int CurrentSettingIndex
    {
        get => currentSettingIndex;
        set
        {
            currentSettingIndex = value;
            currentSettingIndex = currentSettingIndex < 0 ? settingsBars.Length - 1 : currentSettingIndex; 
            currentSettingIndex = currentSettingIndex > settingsBars.Length - 1 ? 0 : currentSettingIndex; 
        }
    }

    private void OnEnable()
    {
        repeatDelay = FindObjectOfType<StandaloneInputModule>().repeatDelay;
        if (settingsBars == null || settingsBars.Length <= 0) return;
        settingsBars[CurrentSettingIndex].SelectBar();
    }

    private void Update()
    {
        if (Input.GetAxis("Vertical") > 0 && lastUpdate + repeatDelay < Time.time)
        {
            settingsBars[currentSettingIndex].OnDeselect();
            CurrentSettingIndex++;
            settingsBars[currentSettingIndex].SelectBar();
            lastUpdate = Time.time;
        }
        else if (Input.GetAxis("Vertical") < 0 && lastUpdate + repeatDelay < Time.time)
        {
            settingsBars[currentSettingIndex].OnDeselect();
            CurrentSettingIndex--;
            settingsBars[currentSettingIndex].SelectBar();
            lastUpdate = Time.time;
        }
    }
}
