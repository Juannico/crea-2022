using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SensitivitySettingBar : SettingBar
{
    [SerializeField] private Image fillBar;
    [SerializeField] private TextMeshProUGUI valueText;

    private float barValue;
    private PlayerMovement playerMov;

    public float BarValue
    {
        get => barValue;
        set
        {
            barValue = value;
            barValue = Mathf.Clamp(value, 1, 10);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        BarValue = PlayerPrefs.GetFloat("Sensitivity");
        playerMov = FindObjectOfType<PlayerMovement>();
        UpdateTextAndProgressValue();
    }

    public override void OnLeftPressed()
    {
        base.OnLeftPressed();
        BarValue -= 0.5f;
        PlayerPrefs.SetFloat("Sensitivity", BarValue);
        if (playerMov != null) playerMov.Sensibilidad = BarValue;
        UpdateTextAndProgressValue();
    }

    public override void OnRightPressed()
    {
        base.OnRightPressed();
        BarValue += 0.5f;
        PlayerPrefs.SetFloat("Sensitivity", BarValue);
        if (playerMov != null) playerMov.Sensibilidad = BarValue;
        UpdateTextAndProgressValue();
    }

    public void UpdateTextAndProgressValue()
    {
        fillBar.fillAmount = BarValue / 10;
        valueText.text = BarValue.ToString();
    }
}
