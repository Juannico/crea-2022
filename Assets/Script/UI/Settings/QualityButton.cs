using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QualityButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private QualitySettingsBar qualitySettingsBar;
    [SerializeField] private GameObject selectedArrow;
    [SerializeField] private int qualityIndex;

    public int QualityIndex { get => qualityIndex;}

    public void Select()
    {
        OnSelect();
        qualitySettingsBar.ChangeQuality(qualityIndex);
    }

    public void OnSelect()
    {
        selectedArrow.SetActive(true);
    }

    public void Deselect()
    {
        selectedArrow?.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Select();
    }
}
