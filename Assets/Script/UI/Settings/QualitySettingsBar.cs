using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class QualitySettingsBar : SettingBar
{
    [SerializeField] private QualityButton[] qualityButtons;
    private int currentQuality;

    public int CurrentQuality
    {
        get => currentQuality;
        set
        {
            currentQuality = value;
            currentQuality = Mathf.Clamp(currentQuality, 0, qualityButtons.Length - 1);
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        CurrentQuality = QualitySettings.GetQualityLevel();
        foreach (var item in qualityButtons)
        {
            item.Deselect();
            if (item.QualityIndex == CurrentQuality)
            {
                item.Select();
            }
        }
    }

    public void ChangeQuality(int qualityIndex)
    {
        //Need to save this later
        QualitySettings.SetQualityLevel(qualityIndex);
        qualityButtons[currentQuality].OnSelect();
    }

    public override void OnLeftPressed()
    {
        base.OnLeftPressed();
        qualityButtons[currentQuality].Deselect();
        ChangeQuality(--CurrentQuality);
    }

    public override void OnRightPressed()
    {
        base.OnRightPressed();
        qualityButtons[currentQuality].Deselect();
        ChangeQuality(++CurrentQuality);
    }
}
