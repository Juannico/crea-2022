using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingBar : MonoBehaviour
{
    [SerializeField] private GameObject leftArrow;
    [SerializeField] private GameObject rightArrow;
    protected float repeatDelay;
    protected float lastUpdate;

    public bool IsSelected { get; private set; }

    protected virtual void OnEnable()
    {
        leftArrow.AddComponent<Button>().onClick.AddListener(OnLeftPressed);
        rightArrow.AddComponent<Button>().onClick.AddListener(OnRightPressed);
        repeatDelay = FindObjectOfType<StandaloneInputModule>().repeatDelay;
    }

    protected virtual void Update()
    {
        if (!IsSelected) return;
        if (Input.GetAxis("Horizontal") < 0 && lastUpdate + repeatDelay < Time.time)
        {
            OnLeftPressed();
            lastUpdate = Time.time;
        }
        else if (Input.GetAxis("Horizontal") > 0 && lastUpdate + repeatDelay < Time.time)
        {
            OnRightPressed();
            lastUpdate = Time.time;
        }
    }
    public void SelectBar()
    {
        OnSelect();
        IsSelected = true;
    }
    public void OnSelect()
    {
        leftArrow.SetActive(true);
        rightArrow.SetActive(true);
    }

    public void OnDeselect()
    {
        //Comment this while no controiller supported
        //leftArrow.SetActive(false);
        //rightArrow.SetActive(false);
        IsSelected = false;
    }
    public virtual void OnLeftPressed() { }
    public virtual void OnRightPressed() { }
}
