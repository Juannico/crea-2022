using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundSettingBar : SettingBar
{
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Image fillBar;
    [SerializeField] private TextMeshProUGUI valueText;

    private float barValue;
    private float normalizedBarValue;

    public float BarValue { get => barValue;
        set
        {
            barValue = value;
            barValue = Mathf.Clamp(value,-80,20);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audioMixer.GetFloat("mixerVolume", out barValue);
        UpdateTextAndProgressValue();
    }

    public override void OnLeftPressed()
    {
        base.OnLeftPressed();
        BarValue -= 10;
        audioMixer.SetFloat("mixerVolume", BarValue);
        UpdateTextAndProgressValue();
    }

    public override void OnRightPressed()
    {
        base.OnRightPressed();
        BarValue += 10;
        audioMixer.SetFloat("mixerVolume", BarValue);
        UpdateTextAndProgressValue();
    }

    public void UpdateTextAndProgressValue()
    {
        normalizedBarValue = (int)(BarValue + 80) / 10;
        fillBar.fillAmount = normalizedBarValue / 10;
        valueText.text = normalizedBarValue.ToString();
    }
}
