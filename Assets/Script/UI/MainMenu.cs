using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public AudioMixer audiomix;
    public GameObject buttonGroupGameobject;

    private void OnEnable()
    {
        //Selects the first button on the button group
        buttonGroupGameobject.transform.GetChild(0).GetComponent<Selectable>().Select();
    }

    public void Caragarjuego()
    {
       SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}

