using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;

    private Audio audioManager;
    // Start is called before the first frame update
    void Start()
    {
        audioManager = FindObjectOfType<Audio>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            HandlePause();
        }
    }

    public void HandlePause()
    {
        if (Time.timeScale <= 0.01)
            Unpause();
        else
            Pause();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        pausePanel?.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        audioManager?.TransitionToPause();
    }

    public void Unpause()
    {
        Time.timeScale = 1;
        pausePanel?.SetActive(false);
        audioManager?.TransitionToUnpaused();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        //UnMufflesouind
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadSceneAsync(0);
    }
}
