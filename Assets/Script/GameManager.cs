using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private UIManager uiManager { get => FindObjectOfType<UIManager>(); }


    [SerializeField] private GameObject flockPrefab;
    private List<GameObject> flocks = new List<GameObject>();
    [SerializeField] private GameObject flockGroup;
    [SerializeField] private GameObject _levels;
    [SerializeField] private ComplexCinematic finalCinematic; 
    public LevelManagement[] levels { get => setLevelsArray(); }
    private LevelManagement _currentLevel;
    public LevelManagement currentLevel { get => _currentLevel; }
    private int currentLevelindex;
    private Dirty[] allDirtyGameobjects;
    public GameObject endPanel;

    private void Start()
    {
        allDirtyGameobjects = FindObjectsOfType<Dirty>();
    }
    // Actualiza el nivel 
    public void CurrentLevel(int level)
    {
        _currentLevel = levels[level - 1];
        currentLevelindex = level - 1;
    }
    // Crea una lista de todos los niveles 
    private LevelManagement[] setLevelsArray()
    {
        LevelManagement[] levels;
        levels = _levels.GetComponentsInChildren<LevelManagement>();
        return levels;
    }
    //Desbloquea el siguiente nivel
    public void unlockLevel()
    {
        currentLevel.cleanDirty();
        uiManager.StartAlert("New area unlocked ", 1.7f);
        levels[currentLevelindex + 1].limitZone.SetActive(false);
    }

    public void CheckIfDone()
    {
        foreach (var item in allDirtyGameobjects)
        {
            if (item.IsDirty) return;
        }
        ActivateFinalCinematic();
    }

    public void ActivateFinalCinematic()
    {
        finalCinematic.OnCinematicFinished += () =>
        {
            endPanel.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        };
        finalCinematic.ActivateCinematic();
    }
}
