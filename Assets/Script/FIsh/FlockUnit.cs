using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockUnit : MonoBehaviour
{
    // flock system sacado de GameDevChef: https://www.youtube.com/watch?v=mBVarJm3Tgk&t=1700s

    [SerializeField] private float _FOVAngle;
    public float FOVAngle { get { return _FOVAngle; } }

    [SerializeField] private float _smoothDamp;
    public float smoothDamp { get { return _smoothDamp; } }

    [SerializeField] private LayerMask _obstacleMask;
    private int obstacleMask { get { return _obstacleMask; } }
    [SerializeField] private Vector3[] directionsToCheckWhenAvodingObstabcle;
    public Vector3 currenVelocity { get; set; }
    private Vector3 currentObstacleAvoidanceVec;
    public float speed { get; set; }


    private Flock assignedFlock;

    public void AssingFlock(Flock flock)
    {
        assignedFlock = flock;
    }

    public void InitilazedSpeed(float speed)
    {
        this.speed = speed;

    }
    public Vector3 CalculateObstacleVec()
    {
        Vector3 obstacleVec = Vector3.zero;
        RaycastHit hit;
        // Cambia de direccion si encuentra un objeto con una distancia maxima y perteneciente al layer "obstableMask", de lo contrario sigue su curso normal
        if (Physics.Raycast(transform.position, transform.forward, out hit, assignedFlock.obstacleDistance, obstacleMask)) obstacleVec = FindBestWaytoAvoidObstable();
        else currentObstacleAvoidanceVec = Vector3.zero;
        obstacleVec *= assignedFlock.obstacleWeight;

        return obstacleVec;
    }

    public Vector3 FindBestWaytoAvoidObstable()
    {
        // Si sigue viendo el mismo objeto mantine la misma direccion
        if (currentObstacleAvoidanceVec != Vector3.zero)
        {
            RaycastHit hit;
            if (!Physics.Raycast(transform.position, transform.forward, out hit, assignedFlock.obstacleDistance, obstacleMask)) return currentObstacleAvoidanceVec;
        }
        float maxDistance = int.MinValue;
        var selectedDirection = Vector3.zero;
        // Calcuala x cantidad de dirreciones y escoge la distancia mas  alejada del obstaculo 
        for (int i = 0; i < directionsToCheckWhenAvodingObstabcle.Length; i++)
        {
            RaycastHit hit;
            var currentDirection = transform.TransformDirection(directionsToCheckWhenAvodingObstabcle[i].normalized);
            if (Physics.Raycast(transform.position, currentDirection, out hit, assignedFlock.obstacleDistance, obstacleMask))
            {
                float currentDistance = (hit.point - transform.position).sqrMagnitude;
                if (currentDistance > maxDistance)
                {
                    maxDistance = currentDistance;
                    selectedDirection = currentDirection;
                }
            }
            else
            {
                selectedDirection = currentDirection;
                currentObstacleAvoidanceVec = currentDirection.normalized;
                return selectedDirection.normalized;
            }
        }
        return selectedDirection.normalized;
    }

}
