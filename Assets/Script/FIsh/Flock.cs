using System.Collections;

using UnityEngine;
using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif  
[System.Serializable]
public class Flock : MonoBehaviour
{
    [Header("Spawn Setup")]
    [SerializeField] private int fishAmount;
    [SerializeField] private FlockUnit[] fishPrefabs;
    private FlockUnit currentFishPrefab;
    [SerializeField] private Vector3 spawnLimit;
    [HideInInspector] public FlockUnit[] allUnit;

    [SerializeField] private Vector3 spawnDirection;
    [SerializeField] private float spawnTime;
    private float spawnWeight = 20;
    [SerializeField] bool randomValues;


    [HideInInspector] public float minSpeed;
    [HideInInspector] public float maxSpeed;
    [HideInInspector] public float cohesionDistance;
    [HideInInspector] public float avoidanceDistance;
    [HideInInspector] public float aligementDistance;
    [HideInInspector] public float limitDistance;
    [HideInInspector] public float obstacleDistance;
    [HideInInspector] public float cohesionWeight;
    [HideInInspector] public float avoidanceWeight;
    [HideInInspector] public float aligementWeight;
    [HideInInspector] public float limitWeight;
    [HideInInspector] public float obstacleWeight;
    // custom editor random values
    #region Editor
#if UNITY_EDITOR
    [CustomEditor(typeof(Flock))]
    public class FlockAttributesEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Flock flock = (Flock)target;


            if (!flock.randomValues)
            {
                ShowAttr(flock);
            }
            EditorUtility.SetDirty(target);
        }
        public void ShowAttr(Flock flock)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("FLOCK ATTRIBUTES STEUP", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("Speed Steup", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Min ", GUILayout.MaxWidth(55));
            flock.minSpeed = EditorGUILayout.Slider(flock.minSpeed, 0, 10);
            EditorGUILayout.LabelField("Max ", GUILayout.MaxWidth(55));
            flock.maxSpeed = EditorGUILayout.Slider(flock.maxSpeed, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Cohesion", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance", GUILayout.MaxWidth(100));
            flock.cohesionDistance = EditorGUILayout.Slider(flock.cohesionDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(100));
            flock.cohesionWeight = EditorGUILayout.Slider(flock.cohesionWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Avoidance", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance", GUILayout.MaxWidth(100));
            flock.avoidanceDistance = EditorGUILayout.Slider(flock.avoidanceDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(100));
            flock.avoidanceWeight = EditorGUILayout.Slider(flock.avoidanceWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Aligement", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance", GUILayout.MaxWidth(100));
            flock.aligementDistance = EditorGUILayout.Slider(flock.aligementDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(100));
            flock.aligementWeight = EditorGUILayout.Slider(flock.aligementWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Limit", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance", GUILayout.MaxWidth(100));
            flock.limitDistance = EditorGUILayout.Slider(flock.limitDistance, 0, 100);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(100));
            flock.limitWeight = EditorGUILayout.Slider(flock.limitWeight, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Obstacle", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Distance", GUILayout.MaxWidth(100));
            flock.obstacleDistance = EditorGUILayout.Slider(flock.obstacleDistance, 0, 10);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(100));
            flock.obstacleWeight = EditorGUILayout.Slider(flock.obstacleWeight, 0, 100);
            EditorGUILayout.EndHorizontal();
        }
    }

#endif
    #endregion
    /*
    public float minSpeed { get { return _minSpeed; } }
    public float maxSpeed { get { return _maxSpeed; } }
    public float cohesionDistance { get { return _cohesionDistance; } }
    public float avoidanceDistance { get { return _avoidanceDistance; } }
    public float aligementDistance { get { return _aligementDistance; } }

    public float obstacleDistance { get { return _obstacleDistance; } }
    public float cohesionWeight { get { return _cohesionWeight; } }
    public float avoidanceWeight { get { return _avoidanceWeight; } }
    public float limitDistance { get { return _limitDistance; } }
    public float aligementWeight { get { return _aligementWeight; } }
    public float limitWeight { get { return _limitWeight; } }
    public float obstacletWeight { get { return _obstacleWeight; } }
    */

    void Start()
    {
        CreateFlock();
    }

    void Update()
    {
        ///Summary//
        //Crea las variables para pasarlas al Job System 
        NativeArray<Vector3> unitForwardDirections = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> unitCurrentVelocity = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> unitPosition = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> cohesionNeighbors = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> avoidanceNeighbors = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> aligementNeightbors = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> aligementNeightborsDirections = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<Vector3> obstacleVecs = new NativeArray<Vector3>(allUnit.Length, Allocator.TempJob);
        NativeArray<float> allUnitsSpeeds = new NativeArray<float>(allUnit.Length, Allocator.TempJob);
        NativeArray<float> neighborsSpeeds = new NativeArray<float>(allUnit.Length, Allocator.TempJob);

        //Asigan los valores
        for (int i = 0; i < allUnit.Length; i++)
        {
            unitForwardDirections[i] = allUnit[i].transform.forward;
            unitCurrentVelocity[i] = allUnit[i].currenVelocity;
            unitPosition[i] = allUnit[i].transform.position;
            cohesionNeighbors[i] = Vector3.zero;
            avoidanceNeighbors[i] = Vector3.zero;
            aligementNeightbors[i] = Vector3.zero;
            aligementNeightborsDirections[i] = Vector3.zero;
            allUnitsSpeeds[i] = allUnit[i].speed;
            neighborsSpeeds[i] = 0f;
            obstacleVecs[i] = allUnit[i].CalculateObstacleVec();
        }
        // Pasa los valores a las variables del Job System
        MoveJob moveJob = new MoveJob
        {
            unitForwardDirections = unitForwardDirections,
            unitCurrentVelocity = unitCurrentVelocity,
            unitPosition = unitPosition,
            cohesionNeighbors = cohesionNeighbors,
            avoidanceNeighbors = avoidanceNeighbors,
            aligementNeightbors = aligementNeightbors,
            aligementNeightborsDirections = aligementNeightborsDirections,
            allUnitsSpeeds = allUnitsSpeeds,
            neighborsSpeeds = neighborsSpeeds,
            obstacleVecs = obstacleVecs,
            cohesionDistance = cohesionDistance,
            avoidanceDistance = avoidanceDistance,
            aligementDistance = aligementDistance,
            limitDistance = limitDistance,
            obstacleDistance = obstacleDistance,
            cohesionWeight = cohesionWeight,
            avoidanceWeight = avoidanceWeight,
            aligementWeight = aligementWeight,
            limitWeight = limitWeight,
            obstacleWeight = obstacleWeight,
            fovAngle = currentFishPrefab.FOVAngle,
            minSpeed = minSpeed,
            maxSpeed = maxSpeed,
            smoothDamp = currentFishPrefab.smoothDamp,
            flockPosition = transform.position,
            deltaTime = Time.deltaTime,
            spawnDirection = spawnDirection.normalized * spawnWeight,
        };
        //Comienza el Job System
        JobHandle handle = moveJob.Schedule(allUnit.Length, 5);
        handle.Complete();
        for (int i = 0; i < allUnit.Length; i++)
        {
            allUnit[i].transform.forward = unitForwardDirections[i];
            allUnit[i].transform.position = unitPosition[i];
            allUnit[i].currenVelocity = unitCurrentVelocity[i];
            allUnit[i].speed = allUnitsSpeeds[i];
        }
        //limbia las variables
        unitForwardDirections.Dispose();
        unitCurrentVelocity.Dispose();
        unitPosition.Dispose();
        cohesionNeighbors.Dispose();
        avoidanceNeighbors.Dispose();
        aligementNeightbors.Dispose();
        aligementNeightborsDirections.Dispose();
        allUnitsSpeeds.Dispose();
        neighborsSpeeds.Dispose();
        obstacleVecs.Dispose();
        ///End Summary//
    }
    public void CreateFlock()
    {
        getPrefab();
        if (randomValues) createAttributesvalues();
        allUnit = new FlockUnit[fishAmount];

        // Instancia x cantidad de objetos 
        for (int i = 0; i < fishAmount; i++)
        {
            var randompositon = Random.insideUnitSphere;
            randompositon = new Vector3(randompositon.x * spawnLimit.x, randompositon.y * spawnLimit.y, randompositon.z * spawnLimit.z);
            var spawnposition = transform.position + randompositon;
            var rotation = new Vector3(0, Random.Range(0, 359), 0);
            allUnit[i] = Instantiate(currentFishPrefab, spawnposition, Quaternion.Euler(rotation));
            allUnit[i].transform.parent = this.transform;
            allUnit[i].AssingFlock(this);
            allUnit[i].InitilazedSpeed(Random.Range(minSpeed, maxSpeed));
        }
        StartCoroutine(Spawn(spawnTime));
    }


    // Solo se activa si en el inspector se deja la variabel Rando values
    private void createAttributesvalues()
    {
        fishAmount = Random.Range(40, 60);
        minSpeed = Random.Range(1, 3.2f);
        maxSpeed = Random.Range(1.2f, 3) + minSpeed;
        cohesionDistance = Random.Range(1, 4.8f);
        avoidanceDistance = Random.Range(1.2f, 5.3f);
        aligementDistance = Random.Range(1, 5);
        limitDistance = Random.Range(30, 80);
        cohesionWeight = Random.Range(2, 5.8f);
        avoidanceWeight = Random.Range(2, 6);
        aligementWeight = Random.Range(2, 6.1f);
        limitWeight = Random.Range(5, 8);

    }
    void getPrefab()
    {
        int index = Random.Range(0, fishPrefabs.Length);
        currentFishPrefab = fishPrefabs[index];

    }

    private IEnumerator Spawn(float time)
    {
        yield return new WaitForSeconds(time);
        spawnWeight = 0;
    }
    // Guia en el unity editor para saber hacia donde se dirigen los peces cuando nacen
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + spawnDirection);
    }
}



//  Unity Jobs System
// flock system sacado de GameDevChef: https://www.youtube.com/watch?v=mBVarJm3Tgk&t=1700s
public struct MoveJob : IJobParallelFor
{
    public NativeArray<Vector3> unitCurrentVelocity;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> unitForwardDirections;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> unitPosition;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> cohesionNeighbors;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> avoidanceNeighbors;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> aligementNeightbors;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> aligementNeightborsDirections;
    [NativeDisableParallelForRestriction] public NativeArray<Vector3> obstacleVecs;
    [NativeDisableParallelForRestriction] public NativeArray<float> allUnitsSpeeds;
    [NativeDisableParallelForRestriction] public NativeArray<float> neighborsSpeeds;

    public Vector3 flockPosition;
    public Vector3 currentObstacleAvoidanceVec;
    public float cohesionDistance;
    public float avoidanceDistance;
    public float aligementDistance;
    public float limitDistance;
    public float obstacleDistance;
    public float cohesionWeight;
    public float avoidanceWeight;
    public float aligementWeight;
    public float limitWeight;
    public float obstacleWeight;
    public float fovAngle;
    public float minSpeed;
    public float maxSpeed;
    public float smoothDamp;
    public float deltaTime;


    public Vector3 spawnDirection;

    public void Execute(int index)
    {
        //Find Neighbours
        int cohesionIndex = 0;
        int avoidanceIndex = 0;
        int aligementIndex = 0;
        for (int i = 0; i < unitPosition.Length; i++)
        {
            Vector3 currentUnitPosition = unitPosition[index];
            Vector3 currentNeighbourPosition = unitPosition[i];
            Vector3 currentNeighbourDirection = unitForwardDirections[i];
            // Agrega "vecinos " si se encuentra en la distancia necesaria de  acudero a cada comportamiento
            if (currentUnitPosition != currentNeighbourPosition)
            {
                float currentDistanceToNeighbourSqr = Vector3.SqrMagnitude(currentUnitPosition - currentNeighbourPosition);
                if (currentDistanceToNeighbourSqr < cohesionDistance * cohesionDistance)
                {
                    cohesionNeighbors[cohesionIndex] = currentNeighbourPosition;
                    neighborsSpeeds[cohesionIndex] = allUnitsSpeeds[i];
                    cohesionIndex++;
                }
                if (currentDistanceToNeighbourSqr < avoidanceDistance * avoidanceDistance)
                {
                    avoidanceNeighbors[avoidanceIndex] = currentNeighbourPosition;
                    avoidanceIndex++;
                }
                if (currentDistanceToNeighbourSqr < aligementDistance * aligementDistance)
                {
                    aligementNeightbors[aligementIndex] = currentNeighbourPosition;
                    aligementNeightborsDirections[aligementIndex] = currentNeighbourDirection;
                    aligementIndex++;
                }
            }
        }

        //Calculate speed
        float speed = 0f;
        if (cohesionNeighbors.Length != 0)
        {
            for (int i = 0; i < cohesionNeighbors.Length; i++)
            {
                speed += neighborsSpeeds[i];
            }
            speed /= cohesionNeighbors.Length;

        }
        speed = Mathf.Clamp(speed, minSpeed, maxSpeed);

        if (spawnDirection != Vector3.zero)
            speed *= 2.5f;
        //Calculate cohesion
        // Comportamiento en donde los peces se juntan en un grupo central dentro del cardumen
        Vector3 cohesionVec = Vector3.zero;
        if (cohesionNeighbors.Length != 0)
        {
            int cohesionNeighbourdInFOV = 0;
            for (int i = 0; i <= cohesionIndex; i++)
            {
                // compara cada "vecino", aumenta su cantidad y suma sus posiciones si estan dentro del rango de vista
                if (IsInFOV(unitForwardDirections[index], unitPosition[index], cohesionNeighbors[i], fovAngle) && cohesionNeighbors[i] != Vector3.zero)
                {
                    cohesionNeighbourdInFOV++;
                    cohesionVec += cohesionNeighbors[i];
                }
            }
            // calcula el vector/posicion central del cardumen
            cohesionVec /= cohesionNeighbourdInFOV;
            cohesionVec -= unitPosition[index];
            cohesionVec = cohesionVec.normalized * cohesionWeight;
        }

        //Calculate avoidance
        //Comportamiento en donde los peces se alejan uno del otro 
        Vector3 avoidanceVec = Vector3.zero;
        if (avoidanceNeighbors.Length != 0)
        {
            int avoidanceNeighbourdInFOV = 0;
            for (int i = 0; i <= avoidanceIndex; i++)
            {
                // compara cada "vecino", aumenta su cantidad y suma al resta del la posicion del peces con su "vecino"
                if (IsInFOV(unitForwardDirections[index], unitPosition[index], avoidanceNeighbors[i], fovAngle) && avoidanceNeighbors[i] != Vector3.zero)
                {
                    avoidanceNeighbourdInFOV++;
                    avoidanceVec += (unitPosition[index] - avoidanceNeighbors[i]);
                }
            }
            // calcula el vector/poscicion para alejarce de los "vecinos"
            avoidanceVec /= avoidanceNeighbourdInFOV;
            avoidanceVec = avoidanceVec.normalized * avoidanceWeight;
        }

        //Calculate aligement
        //Comportamiento en donde los peces se dirence en una misma direccion
        Vector3 aligementVec = Vector3.zero;
        if (aligementNeightbors.Length != 0)
        {
            int aligementNeighbourdInFOV = 0;
            for (int i = 0; i <= aligementIndex; i++)
            {
                //compara "cada" vecino  y suma la direcion en la que se dirigen
                if (IsInFOV(unitForwardDirections[index], unitPosition[index], aligementNeightbors[i], fovAngle) && aligementNeightbors[i] != Vector3.zero)
                {
                    aligementNeighbourdInFOV++;
                    aligementVec += aligementNeightborsDirections[i].normalized;
                }
            }
            // calcula el vector/poscicion a la que se dirigen todos los peces
            aligementVec /= aligementNeighbourdInFOV;
            aligementVec = aligementVec.normalized * aligementWeight;
        }

        //Calculate los limites hasata donde se pueden mover los peces
        Vector3 offsetToCenter = flockPosition - unitPosition[index];
        bool isNearBound = offsetToCenter.magnitude >= limitDistance * 0.9f;
        Vector3 limitVec = isNearBound ? offsetToCenter.normalized : Vector3.zero;
        limitVec *= limitWeight;

        //Move Unit
        Vector3 currentVelocity = unitCurrentVelocity[index];
        Vector3 moveVec = cohesionVec + avoidanceVec + aligementVec + limitVec + obstacleVecs[index] + spawnDirection;

        moveVec = Vector3.SmoothDamp(unitForwardDirections[index], moveVec, ref currentVelocity, smoothDamp, 10000, deltaTime);

        moveVec = moveVec.normalized * speed;
        if (moveVec == Vector3.zero)
        {
            moveVec = unitForwardDirections[index];
        }
        unitPosition[index] = unitPosition[index] + moveVec * deltaTime;
        unitForwardDirections[index] = moveVec.normalized;
        allUnitsSpeeds[index] = speed;
        unitCurrentVelocity[index] = currentVelocity;
    }
    // detenca si hay un "vecino" dentro del rango de vision
    private bool IsInFOV(Vector3 forward, Vector3 unitPosition, Vector3 targetPosition, float angle)
    {
        return Vector3.Angle(forward, targetPosition - unitPosition) <= angle;
    }


}