// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Dirty"
{
	Properties
	{
		_noises("noises", 2D) = "white" {}
		_OpacityNoise1("OpacityNoise1", 2D) = "white" {}
		_OpacityNoise2("OpacityNoise2", 2D) = "white" {}
		_Dirty("Dirty", Range( 0 , 1)) = 0
		_DiryValue("Diry Value", Range( 5 , 10)) = 7.64
		_Color1("Color 1", Color) = (0.6415094,0.6415094,0.6415094,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _noises;
		uniform float4 _noises_ST;
		uniform float4 _Color1;
		uniform float _Dirty;
		uniform sampler2D _OpacityNoise1;
		uniform float4 _OpacityNoise1_ST;
		uniform sampler2D _OpacityNoise2;
		uniform float4 _OpacityNoise2_ST;
		uniform float _DiryValue;


		float2 voronoihash100( float2 p )
		{
			
			p = float2( dot( p, float2( 127.1, 311.7 ) ), dot( p, float2( 269.5, 183.3 ) ) );
			return frac( sin( p ) *43758.5453);
		}


		float voronoi100( float2 v, float time, inout float2 id, inout float2 mr, float smoothness, inout float2 smoothId )
		{
			float2 n = floor( v );
			float2 f = frac( v );
			float F1 = 8.0;
			float F2 = 8.0; float2 mg = 0;
			for ( int j = -1; j <= 1; j++ )
			{
				for ( int i = -1; i <= 1; i++ )
			 	{
			 		float2 g = float2( i, j );
			 		float2 o = voronoihash100( n + g );
					o = ( sin( time + o * 6.2831 ) * 0.5 + 0.5 ); float2 r = f - g - o;
					float d = 0.5 * dot( r, r );
			 		if( d<F1 ) {
			 			F2 = F1;
			 			F1 = d; mg = g; mr = r; id = o;
			 		} else if( d<F2 ) {
			 			F2 = d;
			
			 		}
			 	}
			}
			return F1;
		}


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 color23 = IsGammaSpace() ? float4(0.4622642,0.2260707,0,0) : float4(0.1807607,0.04181687,0,0);
			float time100 = 0.0;
			float2 voronoiSmoothId0 = 0;
			float2 uv_noises = i.uv_texcoord * _noises_ST.xy + _noises_ST.zw;
			float2 coords100 = tex2D( _noises, uv_noises ).rg * 1.21;
			float2 id100 = 0;
			float2 uv100 = 0;
			float voroi100 = voronoi100( coords100, time100, id100, uv100, 0, voronoiSmoothId0 );
			float4 blendOpSrc104 = ( color23 * voroi100 );
			float4 blendOpDest104 = _Color1;
			float2 uv_OpacityNoise1 = i.uv_texcoord * _OpacityNoise1_ST.xy + _OpacityNoise1_ST.zw;
			float2 uv_OpacityNoise2 = i.uv_texcoord * _OpacityNoise2_ST.xy + _OpacityNoise2_ST.zw;
			float simplePerlin2D35 = snoise( ( tex2D( _OpacityNoise1, uv_OpacityNoise1 ) * tex2D( _OpacityNoise2, uv_OpacityNoise2 ) ).rg*_DiryValue );
			simplePerlin2D35 = simplePerlin2D35*0.5 + 0.5;
			float4 lerpBlendMode104 = lerp(blendOpDest104,( blendOpSrc104 * blendOpDest104 ),( _Dirty * ( 0.02755952 + simplePerlin2D35 ) ));
			o.Albedo = ( saturate( lerpBlendMode104 )).rgb;
			float temp_output_58_0 = 0.0;
			o.Metallic = temp_output_58_0;
			o.Smoothness = temp_output_58_0;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18912
252;525;1576;527;637.1442;1024.686;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;66;-1143.84,118.6762;Inherit;False;1700.335;713.7438;Dirty mask;7;26;32;35;29;27;28;73;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;26;-1023.093,311.1568;Inherit;True;Property;_OpacityNoise1;OpacityNoise1;1;0;Create;True;0;0;0;True;0;False;25;51155baaa25b09f4787680621078c55a;51155baaa25b09f4787680621078c55a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;28;-833.4799,583.322;Inherit;True;Property;_OpacityNoise2;OpacityNoise2;2;0;Create;True;0;0;0;True;0;False;25;7723c0db536d44044accffb058c70152;7723c0db536d44044accffb058c70152;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;73;-505.2855,334.2664;Inherit;False;Property;_DiryValue;Diry Value;4;0;Create;True;0;0;0;False;0;False;7.64;7.85;5;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-506.0963,474.3533;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;67;-824.4858,-575.3088;Inherit;False;915.6406;507.8203;DIrty Texture;4;25;24;23;100;;1,1,1,1;0;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;35;-144.2654,544.7063;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;7.64;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-492.767,229.8643;Inherit;False;Constant;_AddOpacity;AddOpacity;4;0;Create;True;0;0;0;False;0;False;0.02755952;0.02755952;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;25;-746.1705,-297.1641;Inherit;True;Property;_noises;noises;0;0;Create;True;0;0;0;True;0;False;25;7723c0db536d44044accffb058c70152;7723c0db536d44044accffb058c70152;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;70;242.3437,-132.2398;Inherit;False;Property;_Dirty;Dirty;3;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;32;281.278,560.4336;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;68;-109.0918,-1108.935;Inherit;False;583;408;Trexture base;1;61;;1,1,1,1;0;0
Node;AmplifyShaderEditor.VoronoiNode;100;-409.0983,-360.6277;Inherit;True;0;0;1;0;1;False;1;False;False;False;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;1.21;False;3;FLOAT;0;False;3;FLOAT;0;FLOAT2;1;FLOAT2;2
Node;AmplifyShaderEditor.ColorNode;23;-726.0657,-490.3654;Inherit;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;0;False;0;False;0.4622642,0.2260707,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-211.3137,-485.448;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;825.3349,124.8285;Inherit;True;2;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;61;39.18011,-960.8777;Inherit;False;Property;_Color1;Color 1;5;0;Create;True;0;0;0;False;0;False;0.6415094,0.6415094,0.6415094,0;0.6415094,0.6415094,0.6415094,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;58;2376.443,-216.2816;Inherit;False;Constant;_Float0;Float 0;4;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;86;-216.907,853.1733;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;104;1359.109,-265.6754;Inherit;True;Multiply;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;19;1900.04,-276.5496;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Dirty;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;27;0;26;0
WireConnection;27;1;28;0
WireConnection;35;0;27;0
WireConnection;35;1;73;0
WireConnection;32;0;29;0
WireConnection;32;1;35;0
WireConnection;100;0;25;0
WireConnection;24;0;23;0
WireConnection;24;1;100;0
WireConnection;72;0;70;0
WireConnection;72;1;32;0
WireConnection;104;0;24;0
WireConnection;104;1;61;0
WireConnection;104;2;72;0
WireConnection;19;0;104;0
WireConnection;19;3;58;0
WireConnection;19;4;58;0
ASEEND*/
//CHKSM=A89C3861DA54FD0CBED0BFEE19F82E63876B1248