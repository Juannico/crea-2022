// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GuideShader"
{
	Properties
	{
		_TexturesCom_BrushStrokes2_overlay_S("TexturesCom_BrushStrokes2_overlay_S", 2D) = "white" {}
		_TextureSample3("Texture Sample 3", 2D) = "white" {}
		_TexturesCom_DirtyPaint1_overlay_S("TexturesCom_DirtyPaint1_overlay_S", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform sampler2D _TexturesCom_DirtyPaint1_overlay_S;
		uniform float4 _TexturesCom_DirtyPaint1_overlay_S_ST;
		uniform sampler2D _TexturesCom_BrushStrokes2_overlay_S;
		uniform float4 _TexturesCom_BrushStrokes2_overlay_S_ST;
		uniform sampler2D _TextureSample1;
		uniform float4 _TextureSample1_ST;
		uniform sampler2D _TextureSample3;
		uniform float4 _TextureSample3_ST;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		float2 voronoihash48( float2 p )
		{
			
			p = float2( dot( p, float2( 127.1, 311.7 ) ), dot( p, float2( 269.5, 183.3 ) ) );
			return frac( sin( p ) *43758.5453);
		}


		float voronoi48( float2 v, float time, inout float2 id, inout float2 mr, float smoothness, inout float2 smoothId )
		{
			float2 n = floor( v );
			float2 f = frac( v );
			float F1 = 8.0;
			float F2 = 8.0; float2 mg = 0;
			for ( int j = -1; j <= 1; j++ )
			{
				for ( int i = -1; i <= 1; i++ )
			 	{
			 		float2 g = float2( i, j );
			 		float2 o = voronoihash48( n + g );
					o = ( sin( time + o * 6.2831 ) * 0.5 + 0.5 ); float2 r = f - g - o;
					float d = 0.5 * dot( r, r );
			 		if( d<F1 ) {
			 			F2 = F1;
			 			F1 = d; mg = g; mr = r; id = o;
			 		} else if( d<F2 ) {
			 			F2 = d;
			
			 		}
			 	}
			}
			return F1;
		}


		inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }

		inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }

		inline float valueNoise (float2 uv)
		{
			float2 i = floor(uv);
			float2 f = frac( uv );
			f = f* f * (3.0 - 2.0 * f);
			uv = abs( frac(uv) - 0.5);
			float2 c0 = i + float2( 0.0, 0.0 );
			float2 c1 = i + float2( 1.0, 0.0 );
			float2 c2 = i + float2( 0.0, 1.0 );
			float2 c3 = i + float2( 1.0, 1.0 );
			float r0 = noise_randomValue( c0 );
			float r1 = noise_randomValue( c1 );
			float r2 = noise_randomValue( c2 );
			float r3 = noise_randomValue( c3 );
			float bottomOfGrid = noise_interpolate( r0, r1, f.x );
			float topOfGrid = noise_interpolate( r2, r3, f.x );
			float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
			return t;
		}


		float SimpleNoise(float2 UV)
		{
			float t = 0.0;
			float freq = pow( 2.0, float( 0 ) );
			float amp = pow( 0.5, float( 3 - 0 ) );
			t += valueNoise( UV/freq )*amp;
			freq = pow(2.0, float(1));
			amp = pow(0.5, float(3-1));
			t += valueNoise( UV/freq )*amp;
			freq = pow(2.0, float(2));
			amp = pow(0.5, float(3-2));
			t += valueNoise( UV/freq )*amp;
			return t;
		}


		float2 voronoihash72( float2 p )
		{
			
			p = float2( dot( p, float2( 127.1, 311.7 ) ), dot( p, float2( 269.5, 183.3 ) ) );
			return frac( sin( p ) *43758.5453);
		}


		float voronoi72( float2 v, float time, inout float2 id, inout float2 mr, float smoothness, inout float2 smoothId )
		{
			float2 n = floor( v );
			float2 f = frac( v );
			float F1 = 8.0;
			float F2 = 8.0; float2 mg = 0;
			for ( int j = -1; j <= 1; j++ )
			{
				for ( int i = -1; i <= 1; i++ )
			 	{
			 		float2 g = float2( i, j );
			 		float2 o = voronoihash72( n + g );
					o = ( sin( time + o * 6.2831 ) * 0.5 + 0.5 ); float2 r = f - g - o;
					float d = 0.5 * dot( r, r );
			 		if( d<F1 ) {
			 			F2 = F1;
			 			F1 = d; mg = g; mr = r; id = o;
			 		} else if( d<F2 ) {
			 			F2 = d;
			
			 		}
			 	}
			}
			return F1;
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 color2 = IsGammaSpace() ? float4(0.6681648,0.990566,0.9107578,0) : float4(0.4039835,0.9786729,0.8088684,0);
			o.Albedo = color2.rgb;
			float4 color3 = IsGammaSpace() ? float4(0.4386792,1,0.905829,0) : float4(0.1616076,1,0.7989964,0);
			o.Emission = color3.rgb;
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode13 = tex2D( _TextureSample0, uv_TextureSample0 );
			float2 uv_TexturesCom_DirtyPaint1_overlay_S = i.uv_texcoord * _TexturesCom_DirtyPaint1_overlay_S_ST.xy + _TexturesCom_DirtyPaint1_overlay_S_ST.zw;
			float2 temp_cast_3 = (_SinTime.x).xx;
			float2 uv_TexCoord9 = i.uv_texcoord * tex2D( _TexturesCom_DirtyPaint1_overlay_S, uv_TexturesCom_DirtyPaint1_overlay_S ).rg + temp_cast_3;
			float2 uv_TexturesCom_BrushStrokes2_overlay_S = i.uv_texcoord * _TexturesCom_BrushStrokes2_overlay_S_ST.xy + _TexturesCom_BrushStrokes2_overlay_S_ST.zw;
			float4 tex2DNode5 = tex2D( _TexturesCom_BrushStrokes2_overlay_S, uv_TexturesCom_BrushStrokes2_overlay_S );
			float2 temp_cast_5 = (_CosTime.x).xx;
			float2 uv_TexCoord8 = i.uv_texcoord * tex2DNode5.rg + temp_cast_5;
			float2 uv_TexCoord10 = i.uv_texcoord * uv_TexCoord9 + uv_TexCoord8;
			float simplePerlin2D11 = snoise( uv_TexCoord10 );
			simplePerlin2D11 = simplePerlin2D11*0.5 + 0.5;
			float clampResult41 = clamp( simplePerlin2D11 , 0.1 , 0.8 );
			float time48 = _CosTime.w;
			float2 voronoiSmoothId0 = 0;
			float2 uv_TextureSample1 = i.uv_texcoord * _TextureSample1_ST.xy + _TextureSample1_ST.zw;
			float2 coords48 = tex2D( _TextureSample1, uv_TextureSample1 ).rg * (0.0*_CosTime.w + 8.75);
			float2 id48 = 0;
			float2 uv48 = 0;
			float voroi48 = voronoi48( coords48, time48, id48, uv48, 0, voronoiSmoothId0 );
			float simpleNoise25 = SimpleNoise( ( tex2DNode13 * ( clampResult41 + voroi48 ) ).rg*1.44 );
			float temp_output_1_0_g3 = simpleNoise25;
			float lerpResult33 = lerp( ( ( temp_output_1_0_g3 - floor( ( temp_output_1_0_g3 + 0.5 ) ) ) * 2 ) , simplePerlin2D11 , 0.02);
			float4 temp_cast_8 = (lerpResult33).xxxx;
			float temp_output_34_0 = ( 1.0 - simplePerlin2D11 );
			float lerpResult36 = lerp( simplePerlin2D11 , temp_output_34_0 , temp_output_34_0);
			float4 lerpResult39 = lerp( temp_cast_8 , ( lerpResult36 / ( 1.0 - tex2DNode5 ) ) , ( tex2DNode13 * float4( 0.05660379,0.05660379,0.05660379,0 ) ));
			float time72 = 0.9;
			float2 uv_TextureSample3 = i.uv_texcoord * _TextureSample3_ST.xy + _TextureSample3_ST.zw;
			float mulTime70 = _Time.y * 0.025;
			float2 temp_cast_10 = (mulTime70).xx;
			float2 uv_TexCoord65 = i.uv_texcoord * tex2D( _TextureSample3, uv_TextureSample3 ).rg + temp_cast_10;
			float simplePerlin2D66 = snoise( uv_TexCoord65*15.93 );
			simplePerlin2D66 = simplePerlin2D66*0.5 + 0.5;
			float2 temp_cast_11 = (simplePerlin2D66).xx;
			float2 coords72 = temp_cast_11 * 1.0;
			float2 id72 = 0;
			float2 uv72 = 0;
			float voroi72 = voronoi72( coords72, time72, id72, uv72, 0, voronoiSmoothId0 );
			float4 lerpResult62 = lerp( lerpResult39 , ( tex2DNode13 * voroi72 ) , ( tex2DNode13 * float4( 1,1,1,0 ) ));
			o.Alpha = lerpResult62.r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18912
258;793;1753;355;259.4767;7.413895;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;75;-2685.958,-212.436;Inherit;False;2192.954;1170.273;Comment;13;5;50;48;51;53;41;11;10;9;8;7;4;6;Create BaseNoise;0.3160377,0.8357099,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;5;-2605.911,736.8179;Inherit;True;Property;_TexturesCom_BrushStrokes2_overlay_S;TexturesCom_BrushStrokes2_overlay_S;0;0;Create;True;0;0;0;False;0;False;-1;51155baaa25b09f4787680621078c55a;51155baaa25b09f4787680621078c55a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinTimeNode;6;-2449.558,243.1766;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CosTime;4;-2605.841,524.1491;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;7;-2457.905,-56.32238;Inherit;True;Property;_TexturesCom_DirtyPaint1_overlay_S;TexturesCom_DirtyPaint1_overlay_S;2;0;Create;True;0;0;0;False;0;False;-1;7723c0db536d44044accffb058c70152;7723c0db536d44044accffb058c70152;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-1914.812,244.3657;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;8;-2086.164,464.9192;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;10;-1636.603,86.88701;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScaleAndOffsetNode;50;-1922.878,697.0394;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;8.75;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;11;-1361.296,106.1221;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;51;-1630.361,508.9976;Inherit;True;Property;_TextureSample1;Texture Sample 1;4;0;Create;True;0;0;0;False;0;False;-1;cd460ee4ac5c1e746b7a734cc7cc64dd;cd460ee4ac5c1e746b7a734cc7cc64dd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VoronoiNode;48;-1127.272,432.1212;Inherit;True;0;0;1;0;1;False;1;False;False;False;4;0;FLOAT2;0,0;False;1;FLOAT;-94.13;False;2;FLOAT;1;False;3;FLOAT;0;False;3;FLOAT;0;FLOAT2;1;FLOAT2;2
Node;AmplifyShaderEditor.ClampOpNode;41;-1049.016,56.58191;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.1;False;2;FLOAT;0.8;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;77;-761.7477,1129.897;Inherit;False;1951.287;448.1732;Comment;7;37;39;34;36;38;13;40;Smooth Ligh Noise;0.5235849,0.979905,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;13;-673.8061,1310.767;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;0;False;0;False;-1;8c4a7fca2884fab419769ccc0355c0c1;8c4a7fca2884fab419769ccc0355c0c1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;76;-390.9136,600.0008;Inherit;False;1103.814;296.88;Comment;4;33;29;25;20;Mask Noise;0.1561021,0.05807228,0.2735849,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;74;-715.5037,1745.742;Inherit;False;2266.408;718.6592;noise;7;69;70;64;65;66;72;63;Last Noise;0.3813156,0.2028302,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;-774.8696,78.41397;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-368.5985,646.4168;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;64;-598.2844,2025.864;Inherit;True;Property;_TextureSample3;Texture Sample 3;1;0;Create;True;0;0;0;False;0;False;-1;9fbef4b79ca3b784ba023cb1331520d5;9fbef4b79ca3b784ba023cb1331520d5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;70;-527.2278,2265.972;Inherit;False;1;0;FLOAT;0.025;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;34;-134.2135,1231.92;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;65;-114.6622,2086.682;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;25;-125.0877,632.6453;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1.44;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;36;215.0074,1182.846;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;38;191.8501,1349.505;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;29;187.1149,630.9862;Inherit;True;Sawtooth Wave;-1;;3;289adb816c3ac6d489f255fc3caf5016;0;1;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;66;300.191,2130.195;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;15.93;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;37;605.1991,1184.899;Inherit;True;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.VoronoiNode;72;651.202,2107.904;Inherit;True;0;0;1;0;1;False;1;False;False;False;4;0;FLOAT2;0,0;False;1;FLOAT;0.9;False;2;FLOAT;1;False;3;FLOAT;0;False;3;FLOAT;0;FLOAT2;1;FLOAT2;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;620.6065,1248.466;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.05660379,0.05660379,0.05660379,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;33;439.4962,626.0755;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.02;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;63;1082.77,1856.925;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;69;1076.8,2107.693;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;39;950.7133,1227.698;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0.0471698,0.0471698,0.0471698,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;62;1538.502,1282.874;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;3;409.6753,63.74932;Inherit;False;Constant;_Color1;Color 1;0;0;Create;True;0;0;0;False;0;False;0.4386792,1,0.905829,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;2;564.1886,-366.7071;Inherit;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;0;False;0;False;0.6681648,0.990566,0.9107578,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1795.057,292.4701;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;GuideShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;9;0;7;0
WireConnection;9;1;6;1
WireConnection;8;0;5;0
WireConnection;8;1;4;1
WireConnection;10;0;9;0
WireConnection;10;1;8;0
WireConnection;50;1;4;4
WireConnection;11;0;10;0
WireConnection;48;0;51;0
WireConnection;48;1;4;4
WireConnection;48;2;50;0
WireConnection;41;0;11;0
WireConnection;53;0;41;0
WireConnection;53;1;48;0
WireConnection;20;0;13;0
WireConnection;20;1;53;0
WireConnection;34;0;11;0
WireConnection;65;0;64;0
WireConnection;65;1;70;0
WireConnection;25;0;20;0
WireConnection;36;0;11;0
WireConnection;36;1;34;0
WireConnection;36;2;34;0
WireConnection;38;0;5;0
WireConnection;29;1;25;0
WireConnection;66;0;65;0
WireConnection;37;0;36;0
WireConnection;37;1;38;0
WireConnection;72;0;66;0
WireConnection;40;0;13;0
WireConnection;33;0;29;0
WireConnection;33;1;11;0
WireConnection;63;0;13;0
WireConnection;69;0;13;0
WireConnection;69;1;72;0
WireConnection;39;0;33;0
WireConnection;39;1;37;0
WireConnection;39;2;40;0
WireConnection;62;0;39;0
WireConnection;62;1;69;0
WireConnection;62;2;63;0
WireConnection;0;0;2;0
WireConnection;0;2;3;0
WireConnection;0;9;62;0
ASEEND*/
//CHKSM=79C4B21E4445EA1F4D245E6237149903019BCF50