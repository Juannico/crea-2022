// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Fog"
{
	Properties
	{
		_TexturesCom_BrushStrokes2_overlay_S("TexturesCom_BrushStrokes2_overlay_S", 2D) = "white" {}
		_TexturesCom_DirtyPaint1_overlay_S("TexturesCom_DirtyPaint1_overlay_S", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		sampler2D _Sampler8752;
		uniform sampler2D _TexturesCom_DirtyPaint1_overlay_S;
		uniform float4 _TexturesCom_DirtyPaint1_overlay_S_ST;
		uniform sampler2D _TexturesCom_BrushStrokes2_overlay_S;
		uniform float4 _TexturesCom_BrushStrokes2_overlay_S_ST;
		float4 _Sampler8752_TexelSize;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		float3 CombineSamplesSharp128_g1( float S0, float S1, float S2, float Strength )
		{
			{
			    float3 va = float3( 0.13, 0, ( S1 - S0 ) * Strength );
			    float3 vb = float3( 0, 0.13, ( S2 - S0 ) * Strength );
			    return normalize( cross( va, vb ) );
			}
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float localCalculateUVsSharp110_g1 = ( 0.0 );
			float4 color26 = IsGammaSpace() ? float4(0.2264151,0.2264151,0.2264151,0) : float4(0.04193995,0.04193995,0.04193995,0);
			float2 uv_TexturesCom_DirtyPaint1_overlay_S = i.uv_texcoord * _TexturesCom_DirtyPaint1_overlay_S_ST.xy + _TexturesCom_DirtyPaint1_overlay_S_ST.zw;
			float2 temp_cast_1 = (_SinTime.x).xx;
			float2 uv_TexCoord12 = i.uv_texcoord * tex2D( _TexturesCom_DirtyPaint1_overlay_S, uv_TexturesCom_DirtyPaint1_overlay_S ).rg + temp_cast_1;
			float2 uv_TexturesCom_BrushStrokes2_overlay_S = i.uv_texcoord * _TexturesCom_BrushStrokes2_overlay_S_ST.xy + _TexturesCom_BrushStrokes2_overlay_S_ST.zw;
			float2 temp_cast_3 = (_CosTime.x).xx;
			float2 uv_TexCoord10 = i.uv_texcoord * tex2D( _TexturesCom_BrushStrokes2_overlay_S, uv_TexturesCom_BrushStrokes2_overlay_S ).rg + temp_cast_3;
			float2 uv_TexCoord5 = i.uv_texcoord * uv_TexCoord12 + uv_TexCoord10;
			float simplePerlin2D1 = snoise( uv_TexCoord5*0.9306426 );
			simplePerlin2D1 = simplePerlin2D1*0.5 + 0.5;
			float4 color49 = IsGammaSpace() ? float4(0.2358491,0.2358491,0.2358491,0) : float4(0.04539381,0.04539381,0.04539381,0);
			float mulTime22 = _Time.y * 0.5;
			float2 temp_cast_4 = (( unity_DeltaTime.x * mulTime22 )).xx;
			float2 uv_TexCoord16 = i.uv_texcoord * float2( 3.48,3.64 ) + temp_cast_4;
			float simplePerlin2D15 = snoise( uv_TexCoord16 );
			simplePerlin2D15 = simplePerlin2D15*0.5 + 0.5;
			float4 temp_output_28_0 = ( simplePerlin2D1 + ( color49 * simplePerlin2D15 ) );
			float2 temp_output_85_0_g1 = ( color26 * temp_output_28_0 ).rg;
			float2 UV110_g1 = temp_output_85_0_g1;
			float4 TexelSize110_g1 = _Sampler8752_TexelSize;
			float2 UV0110_g1 = float2( 0,0 );
			float2 UV1110_g1 = float2( 0,0 );
			float2 UV2110_g1 = float2( 0,0 );
			{
			{
			    UV110_g1.y -= TexelSize110_g1.y * 0.5;
			    UV0110_g1 = UV110_g1;
			    UV1110_g1 = UV110_g1 + float2( TexelSize110_g1.x, 0 );
			    UV2110_g1 = UV110_g1 + float2( 0, TexelSize110_g1.y );
			}
			}
			float4 break134_g1 = tex2D( _Sampler8752, UV0110_g1 );
			float S0128_g1 = break134_g1.r;
			float4 break136_g1 = tex2D( _Sampler8752, UV1110_g1 );
			float S1128_g1 = break136_g1.r;
			float4 break138_g1 = tex2D( _Sampler8752, UV2110_g1 );
			float S2128_g1 = break138_g1.r;
			float temp_output_91_0_g1 = 1.5;
			float Strength128_g1 = temp_output_91_0_g1;
			float3 localCombineSamplesSharp128_g1 = CombineSamplesSharp128_g1( S0128_g1 , S1128_g1 , S2128_g1 , Strength128_g1 );
			o.Normal = localCombineSamplesSharp128_g1;
			float4 color7 = IsGammaSpace() ? float4(0.4625756,0.9428763,0.990566,0.2627451) : float4(0.1810219,0.87494,0.9786729,0.2627451);
			float4 color8 = IsGammaSpace() ? float4(0.09198113,0.2015159,0.3679245,0.2392157) : float4(0.008823112,0.03357904,0.1114872,0.2392157);
			float4 lerpResult6 = lerp( color7 , color8 , simplePerlin2D1);
			o.Albedo = lerpResult6.rgb;
			float4 color34 = IsGammaSpace() ? float4(0.3962264,0.3943574,0.3943574,1) : float4(0.130239,0.128948,0.128948,1);
			o.Emission = ( color34 * lerpResult6 ).rgb;
			float temp_output_50_0 = 0.0;
			o.Metallic = temp_output_50_0;
			o.Smoothness = temp_output_50_0;
			float4 color56 = IsGammaSpace() ? float4(0.2358491,0.2358491,0.2358491,0) : float4(0.04539381,0.04539381,0.04539381,0);
			o.Alpha = ( color56 + temp_output_28_0 ).r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows exclude_path:deferred 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18912
118;304;1576;643;4143.58;1429.339;4.918968;True;True
Node;AmplifyShaderEditor.SimpleTimeNode;22;-562.3535,534.0193;Inherit;False;1;0;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.DeltaTime;29;-665.0447,646.6558;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;17;-410.3909,353.5227;Inherit;False;Constant;_Vector0;Vector 0;3;0;Create;True;0;0;0;False;0;False;3.48,3.64;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-357.323,637.4343;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;-1896.981,-814.9308;Inherit;True;Property;_TexturesCom_DirtyPaint1_overlay_S;TexturesCom_DirtyPaint1_overlay_S;1;0;Create;True;0;0;0;False;0;False;-1;7723c0db536d44044accffb058c70152;7723c0db536d44044accffb058c70152;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinTimeNode;11;-1973.181,-389.7632;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-1900.73,61.56767;Inherit;True;Property;_TexturesCom_BrushStrokes2_overlay_S;TexturesCom_BrushStrokes2_overlay_S;0;0;Create;True;0;0;0;False;0;False;-1;51155baaa25b09f4787680621078c55a;51155baaa25b09f4787680621078c55a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CosTime;13;-1966.757,-166.1137;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-1606.664,-484.6239;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;10;-1584.525,-193.036;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;16;-190.2547,440.1429;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;49;376.2741,727.2731;Inherit;False;Constant;_Color4;Color 4;3;0;Create;True;0;0;0;False;0;False;0.2358491,0.2358491,0.2358491,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-1282.687,-380.0084;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;9;-899.1107,310.014;Inherit;False;Constant;_Float0;Float 0;9;0;Create;True;0;0;0;False;0;False;0.9306426;0;0.5;2.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;15;-46.78137,871.2499;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;161.4901,538.9237;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;1;-546.2654,-82.96313;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;-1.46;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;26;476.2033,177.4526;Inherit;False;Constant;_Color2;Color 2;3;0;Create;True;0;0;0;False;0;False;0.2264151,0.2264151,0.2264151,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;8;-1000.902,-855.5684;Inherit;False;Constant;_Color1;Color 1;2;0;Create;True;0;0;0;False;0;False;0.09198113,0.2015159,0.3679245,0.2392157;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;28;472.5573,416.7248;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;7;-984.5918,-664.6262;Inherit;False;Constant;_Color0;Color 0;2;0;Create;True;0;0;0;False;0;False;0.4625756,0.9428763,0.990566,0.2627451;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;56;743.088,214.8379;Inherit;False;Constant;_Color5;Color 5;3;0;Create;True;0;0;0;False;0;False;0.2358491,0.2358491,0.2358491,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;1001.084,674.9358;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;6;-500.0565,-695.8209;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;34;-84.30204,167.4688;Inherit;False;Constant;_Color3;Color 3;3;0;Create;True;0;0;0;False;0;False;0.3962264,0.3943574,0.3943574,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;51;926.2823,-2.571603;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;50;590.6977,-55.006;Inherit;False;Constant;_Float1;Float 1;3;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;274.0068,-41.97857;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;53;1414.114,289.3304;Inherit;False;Constant;_Float2;Float 2;2;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;52;1347.631,368.2475;Inherit;True;Normal From Texture;-1;;1;9728ee98a55193249b513caf9a0f1676;13,149,0,147,0,143,0,141,0,139,0,151,0,137,0,153,0,159,0,157,0,155,0,135,0,108,0;4;87;SAMPLER2D;_Sampler8752;False;85;FLOAT2;0,0;False;74;SAMPLERSTATE;0;False;91;FLOAT;1.5;False;2;FLOAT3;40;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;58;1080.845,307.8159;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1728.427,-6.717083;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Fog;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;ForwardOnly;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;2;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;31;0;29;1
WireConnection;31;1;22;0
WireConnection;12;0;3;0
WireConnection;12;1;11;1
WireConnection;10;0;2;0
WireConnection;10;1;13;1
WireConnection;16;0;17;0
WireConnection;16;1;31;0
WireConnection;5;0;12;0
WireConnection;5;1;10;0
WireConnection;15;0;16;0
WireConnection;46;0;49;0
WireConnection;46;1;15;0
WireConnection;1;0;5;0
WireConnection;1;1;9;0
WireConnection;28;0;1;0
WireConnection;28;1;46;0
WireConnection;47;0;26;0
WireConnection;47;1;28;0
WireConnection;6;0;7;0
WireConnection;6;1;8;0
WireConnection;6;2;1;0
WireConnection;51;0;50;0
WireConnection;41;0;34;0
WireConnection;41;1;6;0
WireConnection;52;85;47;0
WireConnection;58;0;56;0
WireConnection;58;1;28;0
WireConnection;0;0;6;0
WireConnection;0;1;52;40
WireConnection;0;2;41;0
WireConnection;0;3;50;0
WireConnection;0;4;50;0
WireConnection;0;9;58;0
ASEEND*/
//CHKSM=32090B5AB44BDCAA62118AB1A0234A925AAE2957