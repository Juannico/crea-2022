// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "TrashShader"
{
	Properties
	{
		_BaseTexture("Base Texture", 2D) = "white" {}
		_Normal("Height", 2D) = "white" {}
		_Metallic("Metallic", 2D) = "white" {}
		_Normal1("Normal1", 2D) = "bump" {}
		_OpacityNoise1("OpacityNoise1", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_noises("noises", 2D) = "white" {}
		_DiryValue("Diry Value", Range( 0 , 15)) = 8.013973
		_Dirty("Dirty", Range( 0 , 1)) = 1
		_Opacity("Opacity", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Normal;
		uniform sampler2D _BaseTexture;
		uniform float4 _BaseTexture_ST;
		uniform sampler2D _noises;
		uniform float4 _noises_ST;
		uniform float _Dirty;
		uniform sampler2D _OpacityNoise1;
		uniform float4 _OpacityNoise1_ST;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float _DiryValue;
		uniform sampler2D _Normal1;
		uniform float4 _Normal1_ST;
		uniform sampler2D _Metallic;
		uniform float4 _Metallic_ST;
		uniform float _Opacity;


		float2 voronoihash18( float2 p )
		{
			
			p = float2( dot( p, float2( 127.1, 311.7 ) ), dot( p, float2( 269.5, 183.3 ) ) );
			return frac( sin( p ) *43758.5453);
		}


		float voronoi18( float2 v, float time, inout float2 id, inout float2 mr, float smoothness, inout float2 smoothId )
		{
			float2 n = floor( v );
			float2 f = frac( v );
			float F1 = 8.0;
			float F2 = 8.0; float2 mg = 0;
			for ( int j = -1; j <= 1; j++ )
			{
				for ( int i = -1; i <= 1; i++ )
			 	{
			 		float2 g = float2( i, j );
			 		float2 o = voronoihash18( n + g );
					o = ( sin( time + o * 6.2831 ) * 0.5 + 0.5 ); float2 r = f - g - o;
					float d = 0.5 * dot( r, r );
			 		if( d<F1 ) {
			 			F2 = F1;
			 			F1 = d; mg = g; mr = r; id = o;
			 		} else if( d<F2 ) {
			 			F2 = d;
			
			 		}
			 	}
			}
			return F1;
		}


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_BaseTexture = i.uv_texcoord * _BaseTexture_ST.xy + _BaseTexture_ST.zw;
			float4 color19 = IsGammaSpace() ? float4(0.4622642,0.2260707,0,0) : float4(0.1807607,0.04181687,0,0);
			float time18 = 0.0;
			float2 voronoiSmoothId0 = 0;
			float2 uv_noises = i.uv_texcoord * _noises_ST.xy + _noises_ST.zw;
			float2 coords18 = tex2D( _noises, uv_noises ).rg * 1.21;
			float2 id18 = 0;
			float2 uv18 = 0;
			float voroi18 = voronoi18( coords18, time18, id18, uv18, 0, voronoiSmoothId0 );
			float2 uv_OpacityNoise1 = i.uv_texcoord * _OpacityNoise1_ST.xy + _OpacityNoise1_ST.zw;
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float simplePerlin2D13 = snoise( ( tex2D( _OpacityNoise1, uv_OpacityNoise1 ) * tex2D( _TextureSample0, uv_TextureSample0 ) ).rg*_DiryValue );
			simplePerlin2D13 = simplePerlin2D13*0.5 + 0.5;
			float4 lerpResult23 = lerp( tex2D( _BaseTexture, uv_BaseTexture ) , ( color19 * voroi18 ) , ( _Dirty * ( -0.62 + simplePerlin2D13 ) ));
			float2 temp_output_2_0_g7 = ( 1.0 - lerpResult23 ).rg;
			float2 break6_g7 = temp_output_2_0_g7;
			float temp_output_25_0_g7 = ( pow( 0.5 , 3.0 ) * 0.1 );
			float2 appendResult8_g7 = (float2(( break6_g7.x + temp_output_25_0_g7 ) , break6_g7.y));
			float4 tex2DNode14_g7 = tex2D( _Normal, temp_output_2_0_g7 );
			float temp_output_4_0_g7 = 5.0;
			float3 appendResult13_g7 = (float3(1.0 , 0.0 , ( ( tex2D( _Normal, appendResult8_g7 ).g - tex2DNode14_g7.g ) * temp_output_4_0_g7 )));
			float2 appendResult9_g7 = (float2(break6_g7.x , ( break6_g7.y + temp_output_25_0_g7 )));
			float3 appendResult16_g7 = (float3(0.0 , 1.0 , ( ( tex2D( _Normal, appendResult9_g7 ).g - tex2DNode14_g7.g ) * temp_output_4_0_g7 )));
			float3 normalizeResult22_g7 = normalize( cross( appendResult13_g7 , appendResult16_g7 ) );
			float2 uv_Normal1 = i.uv_texcoord * _Normal1_ST.xy + _Normal1_ST.zw;
			o.Normal = BlendNormals( normalizeResult22_g7 , UnpackNormal( tex2D( _Normal1, uv_Normal1 ) ) );
			o.Albedo = lerpResult23.rgb;
			float4 color62 = IsGammaSpace() ? float4(0.3679245,0.3679245,0.3679245,0) : float4(0.1114872,0.1114872,0.1114872,0);
			float2 uv_Metallic = i.uv_texcoord * _Metallic_ST.xy + _Metallic_ST.zw;
			float4 lerpResult61 = lerp( color62 , tex2D( _Metallic, uv_Metallic ) , lerpResult23);
			o.Metallic = lerpResult61.r;
			o.Alpha = step( simplePerlin2D13 , _Opacity );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18912
196;278;1753;629;939.8273;-700.991;1.444815;True;True
Node;AmplifyShaderEditor.CommentaryNode;7;-2741.563,994.658;Inherit;False;1700.335;713.7438;Dirty mask;7;17;14;13;11;10;9;8;;0.3517711,0.3671981,0.4811321,1;0;0
Node;AmplifyShaderEditor.SamplerNode;8;-2620.816,1187.139;Inherit;True;Property;_OpacityNoise1;OpacityNoise1;5;0;Create;True;0;0;0;True;0;False;-1;51155baaa25b09f4787680621078c55a;51155baaa25b09f4787680621078c55a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;9;-2431.203,1459.304;Inherit;True;Property;_TextureSample0;Texture Sample 0;6;0;Create;True;0;0;0;True;0;False;-1;7723c0db536d44044accffb058c70152;7723c0db536d44044accffb058c70152;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;12;-2422.208,300.673;Inherit;False;915.6406;507.8203;DIrty Texture;4;20;19;18;15;;0.6509434,0.3301159,0.119749,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2103.008,1210.248;Inherit;False;Property;_DiryValue;Diry Value;8;0;Create;True;0;0;0;False;0;False;8.013973;7.64;0;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-2103.819,1350.335;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-2090.49,1105.846;Inherit;False;Constant;_AddOpacity;AddOpacity;4;0;Create;True;0;0;0;False;0;False;-0.62;0.02755952;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;13;-1741.988,1420.688;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;7.64;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;15;-2343.893,578.8177;Inherit;True;Property;_noises;noises;7;0;Create;True;0;0;0;True;0;False;-1;7723c0db536d44044accffb058c70152;7723c0db536d44044accffb058c70152;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;16;-1455.591,793.1;Inherit;False;Property;_Dirty;Dirty;9;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.VoronoiNode;18;-2006.821,515.3541;Inherit;True;0;0;1;0;1;False;1;False;False;False;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;1.21;False;3;FLOAT;0;False;3;FLOAT;0;FLOAT2;1;FLOAT2;2
Node;AmplifyShaderEditor.ColorNode;19;-2323.789,385.6164;Inherit;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;0;False;0;False;0.4622642,0.2260707,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-1316.445,1436.415;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;-132.2055,177.1129;Inherit;True;Property;_BaseTexture;Base Texture;0;0;Create;True;0;0;0;False;0;False;-1;84508b93f15f2b64386ec07486afc7a3;74334038b841bf14b88f373e9c17e456;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-1809.036,390.5338;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-881.5712,1000.81;Inherit;True;2;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;23;-676.0627,367.1355;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;48;-399.2246,763.1422;Inherit;False;887.9333;518.7304;Comment;4;47;42;45;26;Create Normal;0.4103774,0.892916,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;47;-374.4599,854.2929;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;27;-374.0759,1680.031;Inherit;True;Property;_Metallic;Metallic;3;0;Create;True;0;0;0;False;0;False;-1;None;c5797a74d1cfc3241b20235814c2b0be;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;62;-405.7538,1423.425;Inherit;False;Constant;_Color1;Color 1;9;0;Create;True;0;0;0;False;0;False;0.3679245,0.3679245,0.3679245,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;63;197.3817,1862.798;Inherit;False;Property;_Opacity;Opacity;10;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;45;-180.9861,839.2797;Inherit;True;NormalCreate;1;;7;e12f7ae19d416b942820e3932b56220f;0;4;1;SAMPLER2D;;False;2;FLOAT2;0,0;False;3;FLOAT;0.5;False;4;FLOAT;5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;26;-172.2867,1100.352;Inherit;True;Property;_Normal1;Normal1;4;0;Create;True;0;0;0;False;0;False;-1;None;2029ac5c1b9a0874bafc4aff48a5b251;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;65;875.0118,1529.941;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;61;91.98968,1538.744;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendNormalsNode;42;205.9919,872.7798;Inherit;True;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;58;1162.828,910.5762;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;TrashShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;11;0;8;0
WireConnection;11;1;9;0
WireConnection;13;0;11;0
WireConnection;13;1;10;0
WireConnection;18;0;15;0
WireConnection;17;0;14;0
WireConnection;17;1;13;0
WireConnection;20;0;19;0
WireConnection;20;1;18;0
WireConnection;21;0;16;0
WireConnection;21;1;17;0
WireConnection;23;0;3;0
WireConnection;23;1;20;0
WireConnection;23;2;21;0
WireConnection;47;0;23;0
WireConnection;45;2;47;0
WireConnection;65;0;13;0
WireConnection;65;1;63;0
WireConnection;61;0;62;0
WireConnection;61;1;27;0
WireConnection;61;2;23;0
WireConnection;42;0;45;0
WireConnection;42;1;26;0
WireConnection;58;0;23;0
WireConnection;58;1;42;0
WireConnection;58;3;61;0
WireConnection;58;9;65;0
ASEEND*/
//CHKSM=19B7EA9B04AC1637A4B999B46C1DB2B6D8A48A60