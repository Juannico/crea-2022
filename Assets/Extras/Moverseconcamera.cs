﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moverseconcamera : MonoBehaviour
{
    Vector3 _CharacterDirection, realativedir;
    Quaternion _CharacterRotation, rot;
    //Animator _CharacterAnim;
    Rigidbody _CharacterRigidbody;
    [SerializeField]
    float TurnSpeed = 8, speed = 6;
    bool _IsWalking;
    [SerializeField]
    Camera VCam;


    void Start()
    {
        //_CharacterAnim = GetComponent<Animator>();
        _CharacterRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        CharacterActions();
    }
    void FixedUpdate()
    {
        CharacterMovement();
    }

    void CharacterMovement()
    {
        //input movimiento
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        _CharacterDirection.Set(h, v, 0);

        // convertir direccion personaje a camara
        realativedir = VCam.transform.TransformDirection(_CharacterDirection);
        realativedir.y = 0f;
        realativedir.Normalize();
    
        //booleano para revisar movimiento
        bool _IsHorizontalChange = !Mathf.Approximately(h, 0f);
        bool _IsVerticalChange = !Mathf.Approximately(v, 0);
        _IsWalking = _IsHorizontalChange || _IsVerticalChange;
        //_CharacterAnim.SetBool("IsWalking", _IsWalking);

        //girar el personaje hacia la rotacion deseada
        Vector3 _DesairdForward = Vector3.RotateTowards(this.transform.forward, realativedir, TurnSpeed * Time.deltaTime, 0);
        //_CharacterRotation = Quaternion.LookRotation(_DesairdForward); //use this for orbaiting camera with no rotation in move

        //rotar personaje con camara
        rot = VCam.transform.rotation;
        rot.x = 0;
        rot.z = 0;
        transform.rotation = rot;
        //RigidBody Direction && Rotation
        _CharacterRigidbody.MovePosition(_CharacterRigidbody.position + realativedir * speed * Time.deltaTime);
        //_CharacterRigidbody.MoveRotation(rot);

    }

    void CharacterActions()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            //_CharacterAnim.SetTrigger("Shoot");
            Debug.Log("shoot");
        }
        else if (Input.GetKeyDown(KeyCode.Space) && _IsWalking)
        {
            //_CharacterAnim.SetTrigger("Shoot");

        }
    }
}
