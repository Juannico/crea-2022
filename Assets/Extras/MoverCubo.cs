﻿
using UnityEngine;
using System.Collections;
public class MoverCubo : MonoBehaviour
{
    public float speed;
    public Transform camara;
    public float frenado;
    /*private void Start()
    {
        camara = Camera.main.transform;
    }*/
    // Update is called once per frame
        void Update()
        {
        /*Vector3 cameraDirection = new Vector3(camara.forward.x, 0f, camara.forward.z);
        Vector3 playerDirection = new Vector3(transform.forward.x, 0f, transform.forward.z);

        if (Vector3.Angle(cameraDirection, playerDirection) > speed)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, camara.rotation, speed);*/
        PlayerMovement();
        }
        
    void PlayerMovement()
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 horizontal = hor * camara.right;
        Vector3 vertical = ver * camara.forward;
        Vector3 playerMovement = (horizontal+vertical)* speed * Time.deltaTime;

        PlayerRotation(playerMovement);
        transform.Translate(playerMovement, Space.World);
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    void PlayerRotation(Vector3 movimiento)
    {
        if(movimiento.magnitude>0)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, camara.rotation, Time.deltaTime * frenado);
            
        }
    }
}
    
